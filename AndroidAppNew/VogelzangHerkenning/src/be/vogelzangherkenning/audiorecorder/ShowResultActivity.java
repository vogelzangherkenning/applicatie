package be.vogelzangherkenning.audiorecorder;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import be.vogelzangherkenning.audiorecorder.gson.Result;
import be.vogelzangherkenning.audiorecorder.help.ShowResultCustomAdapter;

import com.google.gson.Gson;

public class ShowResultActivity extends ListActivity {
	private static final String TAG = "ShowResultActivity";
	private static final String TITLE = "Bird matches:";
	public static final String EXTRA_RESULT_SERVER = "result_server_json";

	private Gson gson;
	private String json;
	private Result result;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(TITLE);
		
		//Get the json
		json = getIntent().getStringExtra(EXTRA_RESULT_SERVER);
		
		//Parse json
		gson = new Gson();
		try {
		result = gson.fromJson(json, Result.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//Print result
		Log.d(TAG, result.toString());
		
		//Set listAdapter
		if (result.getBirds() != null)
			setListAdapter(new ShowResultCustomAdapter(this, result.getBirds()));
	}
}
