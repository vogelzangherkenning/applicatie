package be.vogelzangherkenning.audiorecorder.gson;

public class Bird {
	/**
	 * {
         "BIRDID":308,
         "SAMPLETYPE":"call",
         "BIRDNAME":"Gyr Falcon",
         "FAMILY":"Falconidae",
         "GENUS":"Falco",
         "SPECIES":"rusticolus",
         "pathCost":39448
      },
	 */
	
	private int BIRDID;
	private String SAMPLETYPE;
	private String BIRDNAME;
	private String FAMILY;
	private String GENUS;
	private String SPECIES;
	private int pathCost;
	private int matches;

	public int getBIRDID() {
		return BIRDID;
	}

	public String getSAMPLETYPE() {
		return SAMPLETYPE;
	}

	public String getBIRDNAME() {
		return BIRDNAME;
	}

	public String getFAMILY() {
		return FAMILY;
	}

	public String getGENUS() {
		return GENUS;
	}

	public String getSPECIES() {
		return SPECIES;
	}
	
	public int getPathCost() {
		return pathCost;
	}
	
	public int getMatches() {
		return matches;
	}

	@Override
	public String toString() {
		return "Bird [BIRDID=" + BIRDID + ", SAMPLETYPE=" + SAMPLETYPE
				+ ", BIRDNAME=" + BIRDNAME + ", FAMILY=" + FAMILY + ", GENUS="
				+ GENUS + ", SPECIES=" + SPECIES + ", pathCost=" + pathCost
				+ "]";
	}
}
