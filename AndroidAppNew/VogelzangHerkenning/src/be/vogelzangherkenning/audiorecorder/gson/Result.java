package be.vogelzangherkenning.audiorecorder.gson;

import java.util.List;

public class Result {
	/**
	 * {
		   "filename":"songs/1.wav",
		   "type":"audio/x-wav",
		   "length":"74132",
		   "meta":{
		
		   },
		   "birds":[
		      {
		         "BIRDID":426,
		         "SAMPLETYPE":"song",
		         "BIRDNAME":"Black-tailed Godwit",
		         "FAMILY":"Scolopacidae",
		         "GENUS":"Limosa",
		         "SPECIES":"limosa",
		         "pathCost":38673
		      },
		      
		      ...
		  ]
		}
	 */

	private List<Bird> birds;
	
	public List<Bird> getBirds() {
		return birds;
	}

	@Override
	public String toString() {
		return "Result [birds=" + getBirds() + "]";
	}
}
