package be.vogelzangherkenning.audiorecorder.record;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.media.MediaRecorder;

public class MediaRecord implements IRecord {
	private MediaRecorder myAudioRecorder;
	
	@SuppressLint("InlinedApi")
	public MediaRecord() {
		myAudioRecorder = new MediaRecorder();
		myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
		myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		myAudioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC_ELD);
	}
	
	@Override
	public void setOutputFile(String file) {
		myAudioRecorder.setOutputFile(file);
	}

	@Override
	public void start() {
		try {
			myAudioRecorder.prepare();
			myAudioRecorder.start();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void stop() {
		myAudioRecorder.stop();
		myAudioRecorder.release();
		myAudioRecorder  = null;
	}

}
