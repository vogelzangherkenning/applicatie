package be.vogelzangherkenning.audiorecorder.record;

import be.vogelzangherkenning.audiorecorder.help.ExtAudioRecorder;

public class PCMRecord implements IRecord {
	private ExtAudioRecorder extAudioRecorder;
	
	public PCMRecord() {
		extAudioRecorder = ExtAudioRecorder.getInstanse(false);
	}
	
	@Override
	public void setOutputFile(String file) {
		extAudioRecorder.setOutputFile(file);
	}

	@Override
	public void start() {
		extAudioRecorder.prepare();
		extAudioRecorder.start();
	}

	@Override
	public void stop() {
		extAudioRecorder.stop();
		extAudioRecorder.release();
	}

}
