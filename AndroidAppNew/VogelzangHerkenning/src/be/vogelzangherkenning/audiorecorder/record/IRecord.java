package be.vogelzangherkenning.audiorecorder.record;

public interface IRecord {
	public void setOutputFile(String file);
	public void start();
	public void stop();
}
