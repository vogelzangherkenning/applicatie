package be.vogelzangherkenning.audiorecorder;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import be.vogelzangherkenning.R;
import be.vogelzangherkenning.audiorecorder.help.CurrentWave;
import be.vogelzangherkenning.audiorecorder.record.IRecord;
import be.vogelzangherkenning.audiorecorder.record.PCMRecord;
import be.vogelzangherkenning.audiorecorder.tasks.SendToServerTask;
import be.vogelzangherkenning.ringdroid.RingdroidEditActivity;

public class MainActivity extends Activity {
	private static final String TAG = "MainActivity";
	private Activity activity = this;

	private Button start, stop;
	private IRecord recorder;
	private Button btnCut;
	private Button btnPlay;
	private Button btnSend;

	private CurrentWave currentWave;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		/** Find buttons*/
		start = (Button)findViewById(R.id.start_button);
		stop = (Button)findViewById(R.id.stop_button);

		/** Enable buttons */
		stop.setEnabled(false);

		/** Make currentWave tracker*/
		currentWave = CurrentWave.getInstance();

		/** Make recorder*/
		recorder = new PCMRecord();
		//recorder = new MediaRecord();
		recorder.setOutputFile(currentWave.getCurrentFullPath());

		/** Find btnCut + onClickListener*/
		btnCut = (Button) findViewById(R.id.cut_button);
		btnCut.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(TAG, "click clicked");

				String filename = currentWave.getCurrentFullPath();
				try {
					Intent intent = new Intent(activity, RingdroidEditActivity.class);
					//intent.putExtra("was_get_content_intent", true);
					intent.putExtra("file", filename);
					/*Intent intent = new Intent(Intent.ACTION_EDIT,
							Uri.parse(filename));
					intent.setClassName(
							"be.vogelzangherkenning.ringdroid",
							"be.vogelzangherkenning.ringdroid.RingdroidEditActivity");*/
					startActivityForResult(intent, 1);
				} catch (Exception e) {
					e.printStackTrace();
					Log.e("Ringdroid", "Couldn't start editor");
				}
			}
		});
		
		//Send button
		btnSend = (Button) findViewById(R.id.send_button);
		//Play button
		btnPlay = (Button)findViewById(R.id.play_button);
		
		togglePlayAndCutAndSendButton(false);
	}

	private void togglePlayAndCutAndSendButton(boolean visible) {
		int visibility = View.VISIBLE;
		//Not visible
		if (!visible)
			visibility = View.INVISIBLE;
		
		
		//Set visibility
		btnPlay.setVisibility(visibility);
		btnCut.setVisibility(visibility);
		btnSend.setVisibility(visibility);
	}

	public void start(View view) {
		/** Start recorder*/
		recorder.start();
		/*new RecordAmplitudeTask().execute();*/

		/** Set buttons enabled correctly */
		start.setEnabled(false);
		stop.setEnabled(true);

		/** Show TOAST text*/
		Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_LONG).show();
	}

	public void stop(View view){
		/*Stop recorder*/
		recorder.stop();

		/** Set buttons enabled correctly */
		stop.setEnabled(false);
		
		/** Show edit & send button*/
		togglePlayAndCutAndSendButton(true);
		
		/**Show Toast*/
		Toast.makeText(getApplicationContext(), "Audio recorded successfully",
				Toast.LENGTH_LONG).show();
	}

	public void send(View view){
		/** Send to server*/
		new SendToServerTask(this, currentWave.getCurrentFullPath(), this).execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//Menu options
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void play(View view) throws IllegalArgumentException,  
	SecurityException, IllegalStateException, IOException {
		/** Play outputfile*/
		MediaPlayer m = new MediaPlayer();
		m.setDataSource(currentWave.getCurrentFullPath());
		m.prepare();
		m.start();
		Toast.makeText(getApplicationContext(), "Playing audio", Toast.LENGTH_LONG).show();
	}

	public CurrentWave getCurrentWave() {
		return this.currentWave;
	}

	/**
	 * Start ShowResultActivity and pass the json result text to it
	 * @param resultText The json (with response from server)
	 */
	public void startResultActivity(String resultText) {
		Intent intent = new Intent(this, ShowResultActivity.class);
		intent.putExtra(ShowResultActivity.EXTRA_RESULT_SERVER, resultText);
		startActivity(intent);
	}
	
	/**
	 * Show popup with error
	 * @param message The error message
	 */
	public void showErrorDialog(String message) {
		ErrorDialogFragment error = new ErrorDialogFragment().setMessage(message);
		error.show(getFragmentManager(), TAG);
	}
}
