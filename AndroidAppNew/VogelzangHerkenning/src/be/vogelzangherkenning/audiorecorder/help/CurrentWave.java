package be.vogelzangherkenning.audiorecorder.help;

import android.util.Log;

public class CurrentWave {
	private static final String TAG = "CurrentWave";
	
	public static final String RECORDING_NAME = "myrecording.wav";
	public static final String RECORDING_NAME_CUTTED = "myrecording_cutted.wav";
	
	private static CurrentWave instance = null;
	//private Wave wave = null;
	public boolean cutted = false;
	
	//Constructor
	private CurrentWave() {
		
	}
	
	//Singleton
	public static CurrentWave getInstance() {
		if (instance == null)
			instance = new CurrentWave();
		
		return instance;
	}
	
	/*public Wave getWave() {
		return wave;
	}
	
	public void loadWave(boolean cutted) {
		this.cutted = cutted;

		//Load wav
		//wave = new Wave(getCurrentFullPath());
		
		Log.d(TAG, "wave=" + wave);
	}*/
	
	public void setCutted(boolean cutted) {
		this.cutted = cutted;
	}
	
	public String getCurrentFileName() {
		String name;
		if (! this.cutted)
			name = RECORDING_NAME;
		else
			name = RECORDING_NAME_CUTTED;
		
		Log.d(TAG, "name = " + name);
		
		return name;
	}
	
	public String getCurrentFullPath() {
		String name = getCurrentFileName();
		
		return ExternalStorage.getPath(name);
	}
}
