package be.vogelzangherkenning.audiorecorder.help;

import java.io.FileOutputStream;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Color;

import com.musicg.wave.Wave;

public class WaveRenderer {
	public static final float WAVEFORM_DEFAULT_TIMESTEP = 0.004F;

	public static Bitmap renderWaveform(Wave wave, String filename, boolean writeToFile) {
		return renderWaveform(wave,WAVEFORM_DEFAULT_TIMESTEP,filename, writeToFile, 0, 100);
	}
	
	public static Bitmap renderWaveform(Wave wave, int minValue, int maxValue) {
		return renderWaveform(wave,WAVEFORM_DEFAULT_TIMESTEP, null, false, minValue, maxValue);
	}

	/**
	 * Render a waveform of a wave file
	 *
	 * @param wave  Wave object
	 * @param timeStep      time interval in second, as known as 1/fps
	 * @param filename      output file
	 * @see RGB graphic rendered
	 */
	public static Bitmap renderWaveform(Wave wave, float timeStep, String filename, boolean writeToFile, int minValue, int maxValue) {

		// for signed signals, the middle is 0 (-1 ~ 1)
		double middleLine=0;

		// usually 8bit is unsigned
		if (wave.getWaveHeader().getBitsPerSample()==8){
			// for unsigned signals, the middle is 0.5 (0~1)
			middleLine=0.5;
		}

		double[] nAmplitudes = wave.getNormalizedAmplitudes();
		int width = (int) (nAmplitudes.length / wave.getWaveHeader().getSampleRate() / timeStep);
		int height = 500;
		int middle = height / 2;
		int magnifier = 1000;

		int numSamples = nAmplitudes.length;
		
		Bitmap map = null;
		if (width>0){
			int numSamplePerTimeFrame = numSamples / width;

			int[] scaledPosAmplitudes = new int[width];
			int[] scaledNegAmplitudes = new int[width];

			// width scaling
			for (int i = 0; i < width; i++) {
				double sumPosAmplitude = 0;
				double sumNegAmplitude = 0;
				int startSample=i * numSamplePerTimeFrame;
				for (int j = 0; j < numSamplePerTimeFrame; j++) {
					double a = nAmplitudes[startSample + j];
					if (a > middleLine) {
						sumPosAmplitude += (a-middleLine);
					} else {
						sumNegAmplitude += (a-middleLine);
					}
				}

				int scaledPosAmplitude = (int) (sumPosAmplitude
						/ numSamplePerTimeFrame * magnifier + middle);
				int scaledNegAmplitude = (int) (sumNegAmplitude
						/ numSamplePerTimeFrame * magnifier + middle);

				scaledPosAmplitudes[i] = scaledPosAmplitude;
				scaledNegAmplitudes[i] = scaledNegAmplitude;
			}

			// render wave form image
			map = Bitmap.createBitmap(width, height, Config.RGB_565);

			//Bitmap bmap = new Bitmap
			//BufferedImage bufferedImage = new BufferedImage(width, height,
			//		BufferedImage.TYPE_INT_RGB);

			for (int i=0; i<height; ++i) {
				for (int j=0; j<width; ++j) {
					map.setPixel(j, i, Color.rgb(255, 255, 255));
				}
			}
			//			// set default white background
			//			Graphics2D graphics = bufferedImage.createGraphics();
			//			graphics.setPaint(new Color(255, 255, 255));
			//			graphics.fillRect(0, 0, bufferedImage.getWidth(),
			//					bufferedImage.getHeight());
			// end set default white background

			for (int i = 0; i < width; i++) {
				for (int j = scaledNegAmplitudes[i]; j < scaledPosAmplitudes[i]; j++) {
					int y = height - j;     // j from -ve to +ve, i.e. draw from top to bottom
					if (y < 0) {
						y = 0;
					} else if (y >= height) {
						y = height - 1;
					}
					//bufferedImage.setRGB(i, y, 0);
					//bitm
					map.setPixel(i, y, 0);
				}
			}
			// end render wave form image

			// export image
			//int dotPos = filename.lastIndexOf(".");
			//String extension=filename.substring(dotPos + 1);
			//				ImageIO.write(bufferedImage, extension,
			//						new File(filename));
			if (writeToFile) {
				FileOutputStream out = null;
				try {
					out = new FileOutputStream(filename);
					map.compress(Bitmap.CompressFormat.PNG, 90, out);
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try{
						out.close();
					} catch(Throwable ignore) {}
				}
			}
			// end export image
			
			//
			int start = (int) ((minValue/100.0f) * (float) width);
			int end = (int) ((maxValue/100.0f) * (float) width);
			
			for (int i=0; i<height; ++i) {
				for (int j=start; j<end; ++j) {
					int pixel = map.getPixel(j, i);
					int redValue = Color.red(pixel);
			        int greenValue = Color.green(pixel);
			        //int blueValue = Color.blue(pixel);

					//Set pixel
					map.setPixel(j, i, Color.rgb(redValue, greenValue, 200));
				}
			}
		}
		else{
			System.err.println("renderWaveform error: Empty Wave");
		}
		
		return map;
	}
}
