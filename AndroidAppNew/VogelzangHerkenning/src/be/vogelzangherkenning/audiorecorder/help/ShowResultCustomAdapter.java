package be.vogelzangherkenning.audiorecorder.help;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import be.vogelzangherkenning.R;
import be.vogelzangherkenning.audiorecorder.gson.Bird;

public class ShowResultCustomAdapter extends ArrayAdapter<Bird> {
	private final Context context;
	private final List<Bird> birds;

	public ShowResultCustomAdapter(Context context, List<Bird> birds) {
		super(context, R.layout.activity_show_result_row, birds);
		this.context = context;
		this.birds = birds;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View rowView = inflater.inflate(R.layout.activity_show_result_row, parent, false);
			
		//Set values
	    /* 
		 {
	         "BIRDID":308,
	         "SAMPLETYPE":"call",
	         "BIRDNAME":"Gyr Falcon",
	         "FAMILY":"Falconidae",
	         "GENUS":"Falco",
	         "SPECIES":"rusticolus",
	         "pathCost":39448
	     },
		*/
		Bird bird = birds.get(position);
		updateText(rowView.findViewById(R.id.tvBirdName), bird.getBIRDNAME() + " (" + bird.getBIRDID() + ")");
		updateText(rowView.findViewById(R.id.tvFamily), bird.getFAMILY());
		updateText(rowView.findViewById(R.id.tvGenus), bird.getGENUS());
		updateText(rowView.findViewById(R.id.tvSpecies), bird.getSPECIES());
		updateText(rowView.findViewById(R.id.tvSampleType), bird.getSAMPLETYPE());
		updateText(rowView.findViewById(R.id.tvDebug), "birdid=" + bird.getBIRDID() + 
				",  pathCost=" + bird.getPathCost() + 
				",  matches=" + bird.getMatches());
		
		return rowView;
	}
	
	private void updateText(View view, String text) {
		TextView textView = (TextView) view;
		textView.setText(text);
	}
}
