package be.vogelzangherkenning.audiorecorder.help;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CopyFile {
	@SuppressWarnings("unused")
	private static final String TAG = "CopyFile";

	public static void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

	/**
	 * Copy currentwave to copy trimmed
	 */
	public static void copyCurrentToCutted() {
		/*try {
			copy(
					new File(ExternalStorage.getPath(CurrentWave.RECORDING_NAME)), 
					new File(ExternalStorage.getPath(CurrentWave.RECORDING_NAME_CUTTED))
					);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Log.d(TAG, "Copying done");*/
	}
}
