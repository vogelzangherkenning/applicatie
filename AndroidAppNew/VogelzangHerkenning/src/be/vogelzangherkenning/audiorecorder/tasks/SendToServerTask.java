package be.vogelzangherkenning.audiorecorder.tasks;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import be.vogelzangherkenning.audiorecorder.MainActivity;
import be.vogelzangherkenning.audiorecorder.servers.Servers;

public class SendToServerTask extends AsyncTask<Void, Void, Void> {
	private static final String TAG = "SendToServerTask";
	private static final String ERROR_MSG = "Sending to server failed";
	
	private Context context;
	private MainActivity mainActivity;
	private String resultText;
	private String outputFile;
	
	public SendToServerTask(Context context, String outputFile, MainActivity mainActivity) {
		this.context = context;
		this.outputFile = outputFile;
		this.mainActivity = mainActivity;
		this.resultText = null;
	}
	
	@Override
	protected Void doInBackground(Void... arg0) {
		FileInputStream fileInputStream = null;
		File f =  new File(outputFile);
		byte[] bFile = new byte[(int) f.length()];

		try {
			//convert file into array of bytes
			fileInputStream = new FileInputStream(f);
			fileInputStream.read(bFile);
			fileInputStream.close();

			System.out.println("Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			String server = Servers.getInstance().getCurrentServer(context);
			String request = server+ "/audio";
			
			URL url = new URL(request);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setInstanceFollowRedirects(false);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "audio/x-wav");
			connection.setUseCaches(false);

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.write(bFile);

			//wr.writeBytes(bFile.toString());
			// wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
			
			Log.d(TAG, "Sending done");
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
			StringBuilder builder = new StringBuilder();
			String aux = "";

			while ((aux = in.readLine()) != null) {
				builder.append(aux);
			}

			String text = builder.toString();
			Log.d(TAG, "Result:  " + text);
			resultText = text;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	@Override
	protected void onPostExecute(Void t)
	{
		//There is result
		if (this.resultText != null) {
			Log.d(TAG, "Result showed");
			mainActivity.startResultActivity(resultText);
		} else { //Something failed
			mainActivity.showErrorDialog(ERROR_MSG);
		}
	}
}