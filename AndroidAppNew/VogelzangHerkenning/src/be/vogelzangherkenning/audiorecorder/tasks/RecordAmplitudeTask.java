package be.vogelzangherkenning.audiorecorder.tasks;

import java.util.Vector;

import android.app.Activity;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.util.Log;

import com.jjoe64.graphview.BarGraphView;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries;

public class RecordAmplitudeTask extends AsyncTask<Void, Void, Void> {
	private Activity activity;
	private MediaRecorder myAudioRecorder;
	private Vector<Integer> amplitudes;
	
	public RecordAmplitudeTask(Activity activity, MediaRecorder myAudioRecorder) {
		this.activity = activity;
		this.myAudioRecorder = myAudioRecorder;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		amplitudes = new Vector<Integer>();
		while(myAudioRecorder != null)
		{
			amplitudes.add(myAudioRecorder.getMaxAmplitude());
			//System.out.println("AMPLITUDE : " + myAudioRecorder.getMaxAmplitude());
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	protected void onPostExecute(Void t)
	{
		Log.d("MainActivity", "KAPOT");
		//for(int i : amplitudes)
		//System.out.println("AMPLITUDE : " + i );

		// init example series data
		GraphViewData[] data = new GraphViewData[amplitudes.size()];
		int pos = 0;
		for(int i : amplitudes) {
			data[pos] = new GraphViewData((double) pos, (double) i);
			++pos;
		}

		GraphViewSeries exampleSeries = new GraphViewSeries(data);

		GraphView graphView = new BarGraphView(
				activity // context
				, "GraphViewDemo" // heading
				);
		graphView.addSeries(exampleSeries); // data

		//LinearLayout layout = (LinearLayout) activity.findViewById(R.id.llGraph);
		//layout.addView(graphView);
	}
}
