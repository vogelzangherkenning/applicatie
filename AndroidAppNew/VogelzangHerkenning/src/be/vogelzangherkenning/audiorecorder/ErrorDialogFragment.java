package be.vogelzangherkenning.audiorecorder;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Showing error to user
 */
public class ErrorDialogFragment extends DialogFragment {
	private static final String TITLE = "Error";
	private static final String OK = "OK";
	private String message = "";
	
	public ErrorDialogFragment() {
		super();
	}
	
	public ErrorDialogFragment setMessage(String message) {
		this.message = message;
		return this;
	}
	
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	//Make builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        
        //Add info
        builder.setTitle(TITLE)
               .setIcon(android.R.drawable.stat_notify_error)
               .setMessage(this.message) 
               .setNegativeButton(OK, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                   }
               });
        
        //Create
        return builder.create();
    }
}