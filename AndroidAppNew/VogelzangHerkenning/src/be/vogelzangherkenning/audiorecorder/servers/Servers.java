package be.vogelzangherkenning.audiorecorder.servers;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class Servers implements Iterable<String> {
	private static Servers singleton = null;
	
	private static final String TAG = "Servers";
	public static final String KEY_SERVER = "prefServer";
	public static final String KEY_CUSTOM_SERVER = "prefCustomServer";
	public static final String KEY_CUSTOM_SERVER_ENABLED = "prefUseCustomServer";
	
	private static final int DEFAULT_SERVER = 0;
	
	private List<String> servers;
	
	private Servers() {
		servers = new LinkedList<String>();
		
		servers.add("http://kolibri.evens.eu:2201");
		servers.add("http://school-vogelzang.herokuapp.com");
	}
	
	public static Servers getInstance() {
		if (singleton == null) {
			singleton = new Servers();
		}
		return singleton;
	}

	@Override
	public Iterator<String> iterator() {
		return servers.iterator();
	}
	
	public int getSize() {
		return servers.size();
	}
	
	public String getCurrentServer(Context context) {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
 
		String server = null;
		/** Check if custom server is enabled*/
		if (sharedPrefs.getBoolean(KEY_CUSTOM_SERVER_ENABLED, false)) {
			Log.d(TAG, "Custom server is enabled");
			
			/** Get custom server address*/
			server = sharedPrefs.getString(KEY_CUSTOM_SERVER, servers.get(DEFAULT_SERVER));
		} else {
			/** Get server from defined server list*/
			server = sharedPrefs.getString(KEY_SERVER, servers.get(DEFAULT_SERVER));
		}
		Log.d(TAG, "Server = " + server);
		
		return server;
	}
}
