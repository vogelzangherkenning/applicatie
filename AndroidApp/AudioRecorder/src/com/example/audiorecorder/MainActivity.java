package com.example.audiorecorder;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.audiorecorder.help.CurrentWave;
import com.example.audiorecorder.help.RangeSeekBar;
import com.example.audiorecorder.help.WaveRenderer;
import com.example.audiorecorder.listeners.BtnCutOnClickListener;
import com.example.audiorecorder.listeners.RangeSeekBarListener;
import com.example.audiorecorder.record.IRecord;
import com.example.audiorecorder.record.PCMRecord;
import com.example.audiorecorder.tasks.SendToServerTask;

public class MainActivity extends Activity {
	private static final String TAG = "MainActivity";

	private Button start, stop, play;
	private TextView result;
	private ImageView imgView;

	private IRecord recorder;
	
	private RangeSeekBar<Integer> rangeSeekBar;
	private ViewGroup rangeSeekBarLayout;
	private Button btnCut;
	private Button btnReset;
	
	private CurrentWave currentWave;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		/** Find buttons + textview*/
		start = (Button)findViewById(R.id.start_button);
		stop = (Button)findViewById(R.id.stop_button);
		play = (Button)findViewById(R.id.play_button);
		result = (TextView)findViewById(R.id.result_text);

		/** Enable buttons */
		stop.setEnabled(false);
		play.setEnabled(false);

		/** Make currentWave tracker*/
		currentWave = new CurrentWave();
		
		/** Make recorder*/
		recorder = new PCMRecord();
		//recorder = new MediaRecord();
		recorder.setOutputFile(currentWave.getCurrentFullPath());
		
		/** Fing imageView*/
		imgView = (ImageView) findViewById(R.id.ivWave);
		
		/**Make rangeSeekBar*/
		rangeSeekBar = new RangeSeekBar<Integer>(0, 100, this);
		rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBarListener(this));
		
		/** Add RangeSeekBar to view*/
		rangeSeekBarLayout = (ViewGroup) findViewById(R.id.vg);
		rangeSeekBarLayout.addView(rangeSeekBar);
		
		/** Find btnCut + onClickListener*/
		btnCut = (Button) findViewById(R.id.btnCut);
		btnCut.setOnClickListener(new BtnCutOnClickListener(this));
		btnReset = (Button) findViewById(R.id.btnReset);
		btnReset.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(TAG, "reset clicked");
				currentWave.loadWave(false);
				
				Bitmap bitmap = WaveRenderer.renderWaveform(getCurrentWave().getWave(), 0, 100);
				imgView.setImageBitmap(bitmap);
				rangeSeekBar.setSelectedMinValue(0);
				rangeSeekBar.setSelectedMaxValue(100);
			}
		});
		
		/** Hide preview*/
		setVisiblePreview(false);
	}
		
	public void start(View view) {
		/** Start recorder*/
		recorder.start();
		/*new RecordAmplitudeTask().execute();*/

		/** Set buttons enabled correctly */
		start.setEnabled(false);
		stop.setEnabled(true);
		
		/** H preview*/
		setVisiblePreview(false);

		/** Show TOAST text*/
		Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_LONG).show();
	}

	public void stop(View view){
		/*Stop recorder*/
		recorder.stop();

		/** Set buttons enabled correctly */
		stop.setEnabled(false);
		play.setEnabled(true);
		
		/** Visualize wav*/
		currentWave.loadWave(false);
		Bitmap bitmap = WaveRenderer.renderWaveform(currentWave.getWave(), 0, 100);
		imgView.setImageBitmap(bitmap);
		
		/** Show preview*/
		setVisiblePreview(true);
		
		Toast.makeText(getApplicationContext(), "Audio recorded successfully",
				Toast.LENGTH_LONG).show();
	}

	public void send(View view){
		/** Send to server*/
		new SendToServerTask(this, currentWave.getCurrentFullPath(), result).execute();
		
		/** Hide preview*/
		setVisiblePreview(false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//Menu options
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void play(View view) throws IllegalArgumentException,  
	SecurityException, IllegalStateException, IOException {
		/** Play outputfile*/
		MediaPlayer m = new MediaPlayer();
		m.setDataSource(currentWave.getCurrentFullPath());
		m.prepare();
		m.start();
		Toast.makeText(getApplicationContext(), "Playing audio", Toast.LENGTH_LONG).show();
	}

	public RangeSeekBar<Integer> getRangeSeekBar() {
		return rangeSeekBar;
	}

	public ImageView getImageView() {
		return imgView;
	}
	
	public void setVisiblePreview(boolean visible) {
		int visibleInt = View.VISIBLE;
		if (! visible)
			visibleInt = View.INVISIBLE;
		
		rangeSeekBarLayout.setVisibility(visibleInt);
		btnCut.setVisibility(visibleInt);
		btnReset.setVisibility(visibleInt);
	}
	
	public CurrentWave getCurrentWave() {
		return this.currentWave;
	}
}
