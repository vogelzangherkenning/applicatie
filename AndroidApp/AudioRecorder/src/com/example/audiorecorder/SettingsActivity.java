package com.example.audiorecorder;

import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.util.Log;

import com.example.audiorecorder.servers.Servers;

public class SettingsActivity extends PreferenceActivity {
	private static final String TAG = "SettingsActivity";
	private Servers servers = Servers.getInstance();
	
	private ListPreference listPreferenceCategory;
	private EditTextPreference customServerEditText;
	
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        
        //Add servers to list
        listPreferenceCategory = (ListPreference) findPreference("prefServer");
        CharSequence entries[] = new String[servers.getSize()];
        CharSequence entryValues[] = new String[servers.getSize()];
        int i = 0;
        for (String server : servers) {
            entries[i] = server;
            entryValues[i] = server;
            i++;
        }
        listPreferenceCategory.setEntries(entries);
        listPreferenceCategory.setEntryValues(entryValues);
        listPreferenceCategory.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				Log.d(TAG, "update summary listPreferenceCategory");
				updateSummary(listPreferenceCategory, (String) newValue);
				/** Update value*/
				return true;
			}
		});
        
        //CustomServerEditText
        customServerEditText = (EditTextPreference) findPreference("prefCustomServer");
        customServerEditText.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				Log.d(TAG, "update summary customServerEditText");
				updateSummary(customServerEditText, (String) newValue);
				
				/**Update value*/
				return true;
			}
		});
        
        /** Update the summary*/
        updateSummary(listPreferenceCategory, null);
        updateSummary(customServerEditText, null);
    }
    
    /**
     * 
     * @param pref
     * @param newValue null? --> take value in pref else use newValue
     */
    public void updateSummary(ListPreference pref, String newValue) {
    	String value = pref.getEntry().toString();
    	if (newValue != null)
    		value = newValue;
    	
    	pref.setSummary(value);
    }
    
    /**
     * 
     * @param pref
     * @param newValue null? --> take value in pref else use newValue
     */
    public void updateSummary(EditTextPreference pref, String newValue) {
    	String value = pref.getText().toString();
    	if (newValue != null)
    		value = newValue;
    	
    	pref.setSummary(value);
    }
}