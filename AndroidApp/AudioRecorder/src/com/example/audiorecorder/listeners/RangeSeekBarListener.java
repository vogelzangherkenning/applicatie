package com.example.audiorecorder.listeners;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.example.audiorecorder.MainActivity;
import com.example.audiorecorder.help.RangeSeekBar;
import com.example.audiorecorder.help.RangeSeekBar.OnRangeSeekBarChangeListener;
import com.example.audiorecorder.help.WaveRenderer;

public class RangeSeekBarListener implements OnRangeSeekBarChangeListener<Integer> {
	private MainActivity mainActivity;
	private ImageView imgView;
	
	public RangeSeekBarListener(MainActivity mainAcitivty) {
		this.mainActivity = mainAcitivty;
		this.imgView = mainAcitivty.getImageView();
	}

	@Override
	public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
		/** Render new file*/
		Bitmap bitmap = WaveRenderer.renderWaveform(mainActivity.getCurrentWave().getWave(), minValue, maxValue);
		imgView.setImageBitmap(bitmap);
	}
}
