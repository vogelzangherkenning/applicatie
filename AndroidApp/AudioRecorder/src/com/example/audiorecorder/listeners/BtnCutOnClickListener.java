package com.example.audiorecorder.listeners;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.audiorecorder.MainActivity;
import com.example.audiorecorder.help.CurrentWave;
import com.example.audiorecorder.help.ExternalStorage;
import com.example.audiorecorder.help.RangeSeekBar;
import com.example.audiorecorder.help.WaveRenderer;
import com.musicg.wave.Wave;
import com.musicg.wave.WaveFileManager;

public class BtnCutOnClickListener implements View.OnClickListener {
	private static final String TAG = "BtnCutOnClickListener";
	
	private MainActivity mainActivity;
	private RangeSeekBar<Integer> rangeSeekBar;
	private ImageView imageView;
	
	public BtnCutOnClickListener(MainActivity mainActivity) {
		this.mainActivity = mainActivity;
		rangeSeekBar = mainActivity.getRangeSeekBar();
		imageView = mainActivity.getImageView();
	}
	
	@Override
	public void onClick(View v) {
		Log.d(TAG, "Button cut clicked");
		int minValue = rangeSeekBar.getSelectedMinValue();
		int maxValue = rangeSeekBar.getSelectedMaxValue();

		/** Copy original*/
		Wave wave = mainActivity.getCurrentWave().getWave();
		
		/** Trim + save*/
		wave.trim(wave.length()*(minValue/100.0f), wave.length()*((100-maxValue)/100.0f));
		WaveFileManager waveFileManager = new WaveFileManager(wave);
		waveFileManager.saveWaveAsFile(ExternalStorage.getPath(CurrentWave.RECORDING_NAME_CUTTED));

		mainActivity.getCurrentWave().loadWave(true);
		Log.d(TAG, "voor renderWaveForm");
		Log.d(TAG, "length: " + mainActivity.getCurrentWave().getWave().length());
		Bitmap bitmap = WaveRenderer.renderWaveform(mainActivity.getCurrentWave().getWave(), 0, 100);
		Log.d(TAG, "erna renderWaveForm");
		imageView.setImageBitmap(bitmap);
		rangeSeekBar.setSelectedMinValue(0);
		rangeSeekBar.setSelectedMaxValue(100);
	}
}
