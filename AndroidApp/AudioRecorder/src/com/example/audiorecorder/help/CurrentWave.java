package com.example.audiorecorder.help;

import android.util.Log;

import com.musicg.wave.Wave;

public class CurrentWave {
	private static final String TAG = "CurrentWave";
	
	public static final String RECORDING_NAME = "myrecording.wav";
	public static final String RECORDING_NAME_CUTTED = "myrecording_cutted.wav";
	private Wave wave = null;
	public boolean cutted = false;
	
	public CurrentWave() {
		
	}
	
	public Wave getWave() {
		return wave;
	}
	
	public void loadWave(boolean cutted) {
		this.cutted = cutted;

		//Load wav
		wave = new Wave(getCurrentFullPath());
		
		Log.d(TAG, "wave=" + wave);
	}
	public String getCurrentFileName() {
		String name;
		if (! this.cutted)
			name = RECORDING_NAME;
		else
			name = RECORDING_NAME_CUTTED;
		
		Log.d(TAG, "name = " + name);
		
		return name;
	}
	
	public String getCurrentFullPath() {
		String name = getCurrentFileName();
		
		return ExternalStorage.getPath(name);
	}
}
