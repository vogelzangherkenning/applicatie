package com.example.audiorecorder.help;

import android.os.Environment;

public class ExternalStorage {
	private static final String SLASH = "/";
	
	/**
	 * 
	 * @param fileName Without first slash ("foo.wav")
	 * @return
	 */
	public static String getPath(String fileName) {
		return Environment.getExternalStorageDirectory().getAbsolutePath() + SLASH + fileName;
	}
}
