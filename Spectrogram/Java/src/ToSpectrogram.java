import com.musicg.graphic.GraphicRender;
import com.musicg.wave.Wave;
import com.musicg.wave.extension.Spectrogram;

/**
 * To Spectrogram
 * @url https://sites.google.com/site/musicgapi/User-guide-v1-4-x/render-spectrogram-demo
 */
public class ToSpectrogram {
	private static final String SONG_FOLDERS = "songs/";
	private static final String OUTPUT_FODLER = "images/";

	/**
	 * List of all wav's that needs to tranfsorm to a spectrogram
	 */
	private static String [] wavs = {"1.wav", "call.wav", "call1.wav", "call-prunella-collaris.wav", "buzzard.wav"};

	/**
	 * Main function
	 * @param args The arguments
	 */
	public static void main(String [] args) {
		//Go through all wavs
		for (int i=0; i<wavs.length; ++i) {
			// create a wave object
			Wave wave = new Wave(SONG_FOLDERS + wavs[i]);
			Spectrogram spectrogram = new Spectrogram(wave);

			// Graphic render
			GraphicRender render = new GraphicRender();	
			render.renderSpectrogramData(spectrogram.getAbsoluteSpectrogramData(), 
					OUTPUT_FODLER + wavs[i] + ".spectrogram.absolute.jpg");
			render.renderSpectrogramData(spectrogram.getNormalizedSpectrogramData(), 
					OUTPUT_FODLER + wavs[i] + ".spectrogram.normalized.jpg");

			//Change the spectrogram representation
			int fftSampleSize = 512;
			int overlapFactor = 2;
			spectrogram = new Spectrogram(wave, fftSampleSize, overlapFactor);	
			render.renderSpectrogramData(spectrogram.getAbsoluteSpectrogramData(), 
					OUTPUT_FODLER + wavs[i] + ".spectrogram2.absolute.jpg");
			render.renderSpectrogramData(spectrogram.getNormalizedSpectrogramData(), 
					OUTPUT_FODLER + wavs[i] + ".spectrogram2.normalized.jpg");
		}	
	}
}
