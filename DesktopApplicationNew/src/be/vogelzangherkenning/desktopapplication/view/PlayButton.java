package be.vogelzangherkenning.desktopapplication.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import be.vogelzangherkenning.desktopapplication.Controller;

public class PlayButton extends JButton implements ActionListener {
	private static final long serialVersionUID = 4806537171922746168L;
	private static final String START_PLAYING = "Play";
	private static final String STOP_PLAYING = "Stop";
	
	private boolean isPlaying = false;

	public PlayButton() {
		isPlaying = false;
		setText(START_PLAYING);
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if (isPlaying) {
			//Stop playing
			Controller.getInstance().stopPlaying();
			isPlaying = false;

			setText(START_PLAYING);
		} else {
			//Start playing
			Controller.getInstance().startPlaying();
			isPlaying = true;

			setText(STOP_PLAYING);
		}

	}
}
