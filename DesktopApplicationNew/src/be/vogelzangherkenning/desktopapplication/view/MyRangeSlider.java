package be.vogelzangherkenning.desktopapplication.view;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import be.vogelzangherkenning.desktopapplication.view.help.JRangeSlider;
import be.vogelzangherkenning.desktopapplication.view.visualize.WavePanel;

public class MyRangeSlider extends JRangeSlider implements ChangeListener {
	private static final long serialVersionUID = 2501414122257302685L;
	private static final int MINIMUM = 0;
	private static final int MAXIMUM = 100;
	
	private WavePanel wavePanel;
	
	public MyRangeSlider(WavePanel wavePanel) {
		//Set slider [0-100] and set start position also on [0-100]
		super(MINIMUM, MAXIMUM, MINIMUM, MAXIMUM, JRangeSlider.HORIZONTAL);
		
		//Set wavePanel --> to update selection
		this.wavePanel = wavePanel;
		
		//Add the changeListener
		addChangeListener(this);
	}
	
	@Override
	public void stateChanged(ChangeEvent ce) {
		int highValue = getHighValue();
		int lowValue = getLowValue();
		wavePanel.updateSlider(lowValue, highValue);
	}
}
