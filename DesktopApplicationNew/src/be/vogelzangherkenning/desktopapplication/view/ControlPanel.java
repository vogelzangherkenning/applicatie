package be.vogelzangherkenning.desktopapplication.view;

import java.awt.GridLayout;

import javax.swing.JPanel;

import be.vogelzangherkenning.desktopapplication.view.help.GBC;

/**
 * Class with all the control buttons
 */
public class ControlPanel extends JPanel {
	private static final long serialVersionUID = 8980774297723123340L;
	
	private RecordButton record;
	private LoadFileButton choose;
	private PlayButton play;
	private CutUncutPanel cut;
	private SendPanel send;
	
	public ControlPanel() {
		setLayout(new GridLayout(3, 2));
		//setBackground(Color.GREEN);
		
		/** Make panels/buttons*/
		record = new RecordButton();
		choose = new LoadFileButton();
		play = new PlayButton();
		cut = new CutUncutPanel();
		send = new SendPanel();
		
		/** Add panels*/
		add(record, new GBC(0, 0).setSpan(1, 1).setFill(GBC.HORIZONTAL));
		add(choose, new GBC(1, 0).setSpan(1, 1).setFill(GBC.HORIZONTAL));
		add(play, new GBC(0, 1).setSpan(1, 1).setFill(GBC.HORIZONTAL));
		add(cut, new GBC(1, 1).setSpan(1, 1).setFill(GBC.HORIZONTAL));
		add(send, new GBC(0, 2).setSpan(2, 1).setFill(GBC.HORIZONTAL));
	}
}
