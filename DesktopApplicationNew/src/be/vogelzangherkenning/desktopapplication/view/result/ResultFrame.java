/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.vogelzangherkenning.desktopapplication.view.result;

import be.vogelzangherkenning.desktopapplication.model.ServerResult;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author Riccardo
 */
public class ResultFrame extends JFrame{
    
    private ServerResult serverResult;
    
    public ResultFrame(ServerResult serverResult){
        super("Results");
        setSize(500, 800);
        this.serverResult = serverResult;
        getContentPane().setLayout(new BorderLayout());
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(new ResultPnl(serverResult),BorderLayout.NORTH);
        contentPane.add(new BirdListPanel(serverResult),BorderLayout.CENTER);
        JScrollPane scroll = new JScrollPane(contentPane);
        getContentPane().add(scroll,BorderLayout.CENTER);
        
        
        
        setVisible(true);
        validate();
        repaint();
    }
}
