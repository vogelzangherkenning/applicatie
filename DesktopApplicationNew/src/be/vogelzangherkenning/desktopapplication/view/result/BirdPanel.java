/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.vogelzangherkenning.desktopapplication.view.result;

import be.vogelzangherkenning.desktopapplication.model.BirdResult;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 *
 * @author Riccardo
 */
public class BirdPanel extends JPanel{
    private Border border;
    private BirdResult bird;
    private JLabel lblBirdID,lblSampleType,lblBirdName,lblFamily,lblGenus,lblSpecies,lblMatches;
    private JLabel lblBirdIDText,lblSampleTypeText,lblBirdNameText,lblFamilyText,lblGenusText,lblSpeciesText,lblMatchesText;
    public BirdPanel(BirdResult bird){
        this.bird = bird;
        
        setTexts();
        setValues();
        
        border = BorderFactory.createLineBorder(Color.black);
        this.setBorder(border);
        this.setLayout(new GridLayout(7, 2));
        
        addLabels();
        
        this.setVisible(true);
    }
    private void addLabels(){
        this.add(lblBirdIDText);
        this.add(lblBirdID);
        this.add(lblSampleTypeText);
        this.add(lblSampleType);
        this.add(lblBirdNameText);
        this.add(lblBirdName);
        this.add(lblFamilyText);
        this.add(lblFamily);
        this.add(lblGenusText);
        this.add(lblGenus);
        this.add(lblSpeciesText);
        this.add(lblSpecies);
        this.add(lblMatchesText);
        this.add(lblMatches);
        
    }
    private void setTexts(){
        lblBirdIDText = new JLabel("BirdID :");
        lblSampleTypeText = new JLabel("SampleType :");
        lblBirdNameText = new JLabel("Bird Name :");
        lblFamilyText = new JLabel("Family :");
        lblGenusText = new JLabel("Genus :");
        lblSpeciesText = new JLabel("Species :");
        lblMatchesText = new JLabel("Matches :");
    }
    private void setValues(){
        lblBirdID = new JLabel(bird.getBirdID() + "");
        lblSampleType = new JLabel(bird.getSampleType());
        lblBirdName = new JLabel(bird.getBirdName());
        lblFamily = new JLabel(bird.getFamily());
        lblGenus = new JLabel(bird.getGenus());
        lblSpecies = new JLabel(bird.getSpecies());
        lblMatches = new JLabel(bird.getMatches()+"");
    }
}
