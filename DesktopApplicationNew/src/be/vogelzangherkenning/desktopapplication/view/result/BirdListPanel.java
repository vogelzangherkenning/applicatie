/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.vogelzangherkenning.desktopapplication.view.result;

import be.vogelzangherkenning.desktopapplication.model.ServerResult;
import java.awt.GridLayout;
import javax.swing.JPanel;

/**
 *
 * @author Riccardo
 */
public class BirdListPanel extends JPanel{
    private ServerResult serverResult;
    public BirdListPanel(ServerResult serverResult){
        this.serverResult = serverResult;
        this.setLayout(new GridLayout(serverResult.getBirds().size(), 1));
        for(int i = 0;i<serverResult.getBirds().size();i++){
            this.add(new BirdPnl(serverResult.getBirds().get(i)));
            
        }
        this.setVisible(true);
    }
}
