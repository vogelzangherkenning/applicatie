package be.vogelzangherkenning.desktopapplication.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import be.vogelzangherkenning.desktopapplication.Controller;

/**
 * Load file button with chooser
 */
public class LoadFileButton extends JButton implements ActionListener {
	private static final long serialVersionUID = -3473020009512297540L;
	private static final String LOAD_FILE = "Load File";
	private static JFileChooser chooser;
	
	public LoadFileButton() {
		setText(LOAD_FILE);
		addActionListener(this);
		
		/** Make file chooser*/
		makeFileChooser();
	}
	
	private void makeFileChooser() {
		//Make chooser
		chooser = new JFileChooser();
		
		//Set only accept wav files
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Wav audio files", "wav");
		chooser.setFileFilter(filter);
		
		//Set current directory
		chooser.setCurrentDirectory(new File("."));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int returnVal = chooser.showOpenDialog(null);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			System.out.println("You chose to open this file: "
					+ chooser.getSelectedFile().getAbsolutePath());
		}
		
		//Load file
		Controller.getInstance().loadFile(chooser.getSelectedFile().getAbsolutePath());
	}
}
