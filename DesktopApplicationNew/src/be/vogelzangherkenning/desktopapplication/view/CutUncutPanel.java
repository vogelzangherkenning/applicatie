package be.vogelzangherkenning.desktopapplication.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import be.vogelzangherkenning.desktopapplication.Controller;
import be.vogelzangherkenning.desktopapplication.view.help.GBC;

public class CutUncutPanel extends JPanel {
	private static final long serialVersionUID = -479603115091206483L;
	private static final String CUT = "Cut";
	private static final String UNDO_CUT = "Undo Cut (Reset)";

	private JButton undo;

	public CutUncutPanel() {
		setLayout(new GridLayout(1, 2));

		/** Make buttons*/
		initCutButton();
		initUndoButton();
	}

	private void initCutButton() {
		//Btn "cut"
		JButton cut = new JButton(CUT);
		cut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Controller.getInstance().cut();

				//Let user press undo
				undo.setEnabled(true);
			}
		});

		//Add to panel
		add(cut,new GBC(0, 0));
	}

	private void initUndoButton() {
		//Btn "undo"
		undo = new JButton(UNDO_CUT);
		undo.setEnabled(false);
		undo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controller.getInstance().undoCut();

				//Reset --> So uncheck undo
				undo.setEnabled(false);
			}
		});

		//Add to panel
		add(undo, new GBC(0, 1));
	}
}
