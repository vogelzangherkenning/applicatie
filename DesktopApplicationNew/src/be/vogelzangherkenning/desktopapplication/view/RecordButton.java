package be.vogelzangherkenning.desktopapplication.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import be.vogelzangherkenning.desktopapplication.Controller;

public class RecordButton extends JButton implements ActionListener {
	private static final long serialVersionUID = 9172528908466035993L;
	private static final String START_RECORDING = "Start Recording";
	private static final String STOP_RECORDING = "Stop Recording";
	
	private boolean isRecording = false;

	public RecordButton() {
		isRecording = false;
		setText(START_RECORDING);
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if (isRecording) {
			//Stop recording
			Controller.getInstance().stopRecording();
			isRecording = false;

			setText(START_RECORDING);
		} else {
			//Start recording
			Controller.getInstance().startRecording();
			isRecording = true;

			setText(STOP_RECORDING);
		}

	}
}
