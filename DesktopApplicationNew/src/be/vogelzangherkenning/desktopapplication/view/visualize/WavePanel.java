package be.vogelzangherkenning.desktopapplication.view.visualize;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

/**
 * Created by IntelliJ IDEA.
 * User: Jonathan Simon
 * Date: Mar 6, 2005
 * Time: 9:16:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class WavePanel extends JPanel implements Observer {
	private static final long serialVersionUID = -5339229090539082825L;
	protected static final Color BACKGROUND_COLOR = Color.white;
    protected static final Color REFERENCE_LINE_COLOR = Color.black;
    protected static final Color WAVEFORM_COLOR = Color.red;

    private AudioInfo helper;
    private int channelIndex;

    /** Min Max slider*/
    private int min = 0;
    private int max = 100;
    private float curTimePercentage = 0;
    
    public WavePanel(AudioInfo helper, int channelIndex) {
        this.helper = helper;
        this.channelIndex = channelIndex;
        setBackground(BACKGROUND_COLOR);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        int lineHeight = getHeight() / 2;
        
        /** Draw selected region*/
        g.setColor(new Color(150,180,220));
        int x = (int) (getWidth() * getMinInPercentage());
        int width = ((int) (getMaxMinusMinInPercentage()*getWidth()));
        g.fillRect(x, 0, width, lineHeight*2);
        
        /** Draw horizontal divider*/
        g.setColor(REFERENCE_LINE_COLOR);
        g.drawLine(0, lineHeight, (int)getWidth(), lineHeight);
        
        /** Draw current position in song*/
        float xPos = (float) getWidth() * ((float) curTimePercentage / 100.0f);
        g.setColor(Color.GREEN);
        g.drawLine((int) xPos, 0, (int) xPos, lineHeight*2);
        
        /** Draw the waveform*/
        drawWaveform(g, helper.getAudio(channelIndex));
    }
    
    private float getMinInPercentage() {
    	return (float) min / 100.0f;
    }
    
    private float getMaxInPercentage() {
    	return (float) max / 100.0f;
    }
    
    private float getMaxMinusMinInPercentage() {
    	return getMaxInPercentage() - getMinInPercentage();
    }

    protected void drawWaveform(Graphics g, int[] samples) {
        if (samples == null) {
            return;
        }

        int oldX = 0;
        int oldY = (int) (getHeight() / 2);
        int xIndex = 0;

        int increment = helper.getIncrement(helper.getXScaleFactor(getWidth()));
        g.setColor(WAVEFORM_COLOR);

        int t = 0;

        for (t = 0; t < increment; t += increment) {
            g.drawLine(oldX, oldY, xIndex, oldY);
            xIndex++;
            oldX = xIndex;
        }

        for (; t < samples.length; t += increment) {
            double scaleFactor = helper.getYScaleFactor(getHeight());
            double scaledSample = samples[t] * scaleFactor;
            int y = (int) ((getHeight() / 2) - (scaledSample));
            g.drawLine(oldX, oldY, xIndex, y);

            xIndex++;
            oldX = xIndex;
            oldY = y;
        }
    }

	public void updateSlider(int lowValue, int highValue) {
		min = lowValue;
		max = highValue;
		repaint();
	}

	@Override
	public void update(Observable o, Object arg) {
		//TODO terug opzetten
		/*curTimePercentage = (float) arg;
		invalidate();
		repaint();*/
	}
}

/*   Swing Hacks
 *   Tips and Tools for Killer GUIs
 * By Joshua Marinacci, Chris Adamson
 *   First Edition June 2005
 *   Series: Hacks
 *   ISBN: 0-596-00907-0
 *   http://www.oreilly.com/catalog/swinghks/
 */
