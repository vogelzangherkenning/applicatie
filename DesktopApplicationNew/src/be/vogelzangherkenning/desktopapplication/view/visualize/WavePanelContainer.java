package be.vogelzangherkenning.desktopapplication.view.visualize;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.sound.sampled.AudioInputStream;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import be.vogelzangherkenning.desktopapplication.view.ControlPanel;
import be.vogelzangherkenning.desktopapplication.view.MyRangeSlider;
import be.vogelzangherkenning.desktopapplication.view.help.JRangeSlider;
@SuppressWarnings("all")
public class WavePanelContainer extends JPanel {    
	private static final long serialVersionUID = -3387709730671995307L;
	private ArrayList singleChannelWaveformPanels = new ArrayList();
	private AudioInfo audioInfo = null;
	
	private WavePanel wavePanel;
	private MyRangeSlider slider;
	private JPanel controlPanel = null;

	public WavePanelContainer() {
		setLayout(new GridLayout(0,1));
	}

	public void setAudioToDisplay(AudioInputStream audioInputStream){
		//Remove existing panels
		removeAll();
		
		//Make audioinfo
		singleChannelWaveformPanels = new ArrayList();
		audioInfo = new AudioInfo(audioInputStream);
		
		//Only take channel 0
		WavePanel waveformPanel = new WavePanel(audioInfo, 0);
		singleChannelWaveformPanels.add(waveformPanel);
		
		//Create + add panel
		JPanel panel = createPanel(waveformPanel);
		add(panel);
		
		//Revalidate
		revalidate();
	}
	
	/** Create panel with controls*/
	private JPanel createPanel(WavePanel wavePanel) {
		this.wavePanel = wavePanel;

		/** Add Wave*/
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(wavePanel, BorderLayout.CENTER);
		
		/** If no controlPanel exists make new one*/
		if (controlPanel == null)
			controlPanel = new ControlPanel();
		panel.add(controlPanel, BorderLayout.NORTH);
		
		/** Add slider*/
		slider = new MyRangeSlider(wavePanel);
		panel.add(slider, BorderLayout.SOUTH);

		return panel; 
	} 
	
	public int getSliderHighValue() {
		return slider.getHighValue();
	}
	
	public int getSliderLowValue() {
		return slider.getLowValue();
	}
	
	public WavePanel getWavePanel() {
		return wavePanel;
	}
}