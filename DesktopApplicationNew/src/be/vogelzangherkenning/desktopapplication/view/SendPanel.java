package be.vogelzangherkenning.desktopapplication.view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import be.vogelzangherkenning.desktopapplication.Controller;
import be.vogelzangherkenning.desktopapplication.view.help.ComboItem;
import be.vogelzangherkenning.desktopapplication.view.help.GBC;

public class SendPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = -3859280348368705778L;
	private static final String SEND = "Send";
	
	private JComboBox<ComboItem> comboBoxServers;
	private JButton btnSend;
	
	/** The servers with name and server url*/
	private String [][] servers = {
			{"Kolibri", "http://kolibri.evens.eu:2201"},
			{"Heroku", "http://school-vogelzang.herokuapp.com"}
	};
	
	public SendPanel() {
		setLayout(new GridLayout(1, 2));
		
		//Make comboboxes
		comboBoxServers = new JComboBox<ComboItem>();
		for (int i=0; i<servers.length; ++i)
			comboBoxServers.addItem(new ComboItem(servers[i][0], servers[i][1]));
		
		//Make send button
		btnSend = new JButton(SEND);
		btnSend.addActionListener(this);
		
		//Add to panel
		add(comboBoxServers, new GBC(0, 0));
		add(btnSend, new GBC(0, 1));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ComboItem item = (ComboItem) comboBoxServers.getSelectedItem();
		System.out.println("Send to " + item.getValue());
		
		Controller.getInstance().send(item.getValue());
	}
}
