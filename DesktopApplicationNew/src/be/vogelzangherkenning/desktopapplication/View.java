package be.vogelzangherkenning.desktopapplication;

import java.awt.BorderLayout;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import be.vogelzangherkenning.desktopapplication.model.AudioFunctions;
import be.vogelzangherkenning.desktopapplication.view.visualize.WavePanelContainer;

public class View extends JFrame {
	private static final long serialVersionUID = -407472159675526738L;
	private static final String TITLE = "Vogelzang Herkenning";

	private WavePanelContainer audioContainer;    

	public View() {
		super(TITLE);
		setBounds(200,200, 500, 350);

		try {
			/** Make audio view*/
			audioContainer = new WavePanelContainer();
			audioContainer.setAudioToDisplay(AudioFunctions.getAudioInputStream(Controller.getInstance().getCurrentFile()));
			Controller.getInstance().setView(this);

			/** Add view*/
			setAudioView();
		} catch (Exception e) {
			e.printStackTrace();
		}

		/** Set icon + close operation*/
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		URL url = View.class.getClassLoader().getResource("be/vogelzangherkenning/desktopapplication/icon/button.png");
		ImageIcon icon = new ImageIcon(url);
		setIconImage(icon.getImage());
		redraw();
	}

	public void redraw(){
                setExtendedState(JFrame.MAXIMIZED_BOTH); 
		setVisible(true);
		validate();
		repaint();
	}
	public void setAudioView(){
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(audioContainer, BorderLayout.CENTER);
	}
	
	public WavePanelContainer getAudioContainer() {
		return audioContainer;
	}

	public void showWarning(String msg) {
		JOptionPane.showMessageDialog(this, msg);
	}
}


/*   Swing Hacks
 *   Tips and Tools for Killer GUIs
 * By Joshua Marinacci, Chris Adamson
 *   First Edition June 2005
 *   Series: Hacks
 *   ISBN: 0-596-00907-0
 *   http://www.oreilly.com/catalog/swinghks/
 */
