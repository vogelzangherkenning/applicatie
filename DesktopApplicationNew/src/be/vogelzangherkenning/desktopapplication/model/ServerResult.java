/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.vogelzangherkenning.desktopapplication.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.ArrayList;

/**
 *
 * @author Riccardo
 */
public class ServerResult {
    private boolean succes;
    private int id;
    private String type,length,filename;
    //meta data nog doen
    private ArrayList<BirdResult> birds;
    private JsonObject json;
    
    /** TESTING PURPOSES ONLY **/
    public ServerResult(){
        String jsonString = "{\"success\":true,\"id\":10,\"type\":\"audio/x-wav\",\"length\":\"264620\",\"filename\":\"songs/10.wav\",\"meta\":{},\"birds\":[{\"BIRDID\":122,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Common Bittern\",\"FAMILY\":\"Ardeidae\",\"GENUS\":\"Botaurus\",\"SPECIES\":\"stellaris\",\"pathCost\":52110},{\"BIRDID\":122,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Common Bittern\",\"FAMILY\":\"Ardeidae\",\"GENUS\":\"Botaurus\",\"SPECIES\":\"stellaris\",\"pathCost\":54177},{\"BIRDID\":122,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Common Bittern\",\"FAMILY\":\"Ardeidae\",\"GENUS\":\"Botaurus\",\"SPECIES\":\"stellaris\",\"pathCost\":54694},{\"BIRDID\":122,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Common Bittern\",\"FAMILY\":\"Ardeidae\",\"GENUS\":\"Botaurus\",\"SPECIES\":\"stellaris\",\"pathCost\":55469},{\"BIRDID\":122,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Common Bittern\",\"FAMILY\":\"Ardeidae\",\"GENUS\":\"Botaurus\",\"SPECIES\":\"stellaris\",\"pathCost\":57364},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":57967},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":58570},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":58656},{\"BIRDID\":122,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Common Bittern\",\"FAMILY\":\"Ardeidae\",\"GENUS\":\"Botaurus\",\"SPECIES\":\"stellaris\",\"pathCost\":58914},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":59259},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":61671},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":61671},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":61757},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":62015},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":62360},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":62446},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":62618},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":62790},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":62790},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":63135},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":63479},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":63479},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":63824},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":64168},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":64168},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":65202},{\"BIRDID\":327,\"SAMPLETYPE\":\"song\",\"BIRDNAME\":\"Andalusian Hemipode\",\"FAMILY\":\"Turnicidae\",\"GENUS\":\"Turnix\",\"SPECIES\":\"sylvatica\",\"pathCost\":65891}]}";
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
	json = gson.fromJson(jsonString,JsonObject.class);
    }
    public ServerResult(JsonObject json){
        this.json = json;
        birds = new ArrayList<BirdResult>();
        setValues();
    }
    
    private void setValues(){
        succes = json.get("success").getAsBoolean();
        if(succes){
            id      = json.get("id").getAsInt();
            type    = json.get("type").getAsString();
            length  = json.get("length").getAsString();
            filename= json.get("filename").getAsString();
            JsonArray birdArray = json.get("birds").getAsJsonArray();
            buildBirds(birdArray);
            //sortBirds();
        }
    }
    
    private void buildBirds(JsonArray birdArray){
        for(int i = 0;i<birdArray.size();i++){
            JsonObject bird = birdArray.get(i).getAsJsonObject();
            int location = isAlreadyInArray(bird);
            birds.add(new BirdResult(bird));
            /*if (location>=0){
                birds.get(location).addEntry(bird.get("pathCost").getAsInt());
            }else {
                BirdResult birdRes = new BirdResult(bird);
                birds.add(birdRes);
            }*/
        }
    }
    
    private int isAlreadyInArray(JsonObject bird){
        for(int i =0;i<birds.size();i++){
            if(birds.get(i).getBirdID()== bird.get("BIRDID").getAsInt()){
                return i;
            }
        }
        return -1;
    }
    
    private void sortBirds(){
        for(int i = 0; i< birds.size();i++){
            for(int j = 0; j<birds.size();j++){
                if(j<=birds.size()-2){
                    if(birds.get(j).getBestPathCost() < birds.get(j+1).getBestPathCost()){
                        BirdResult temp = birds.get(j);
                        birds.set(j, birds.get(j+1));
                        birds.set(j+1, temp);
                    }
                }

            }
        }
    }
    
    @Override
    public String toString(){
        String ret = "";
        
        ret += "Succes : " + succes + "\n";
        ret += "id : " + id + "\n";
        ret += "type : " + type + "\n";
        ret += "length : " + length + "\n";
        ret += "filename : " + filename + "\n";
        
        ret+= "BIRDS \n";
        for(int i=0;i<birds.size();i++){
            ret += "\n";
            ret += birds.get(i).toString();
            ret += "\n";
        }
        
        return ret;
    }

    public boolean isSucces() {
        return succes;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getLength() {
        return length;
    }

    public String getFilename() {
        return filename;
    }

    public ArrayList<BirdResult> getBirds() {
        return birds;
    }
    
    
}
