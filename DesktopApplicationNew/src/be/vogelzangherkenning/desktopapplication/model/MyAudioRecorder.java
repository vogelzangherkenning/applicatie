package be.vogelzangherkenning.desktopapplication.model;

import java.io.File;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

public class MyAudioRecorder {
	private AudioRecorder audioRecorder;
	
	public MyAudioRecorder(String fileName) {
		File file = new File(fileName);

		/* For simplicity, the audio data format used for recording
	   is hardcoded here. We use PCM 44.1 kHz, 16 bit signed,
	   stereo.
		 */
		AudioFormat	audioFormat = new AudioFormat(
				AudioFormat.Encoding.PCM_SIGNED,
				44100.0F, 16, 2, 4, 44100.0F, false);

		/* Now, we are trying to get a TargetDataLine. The
		   TargetDataLine is used later to read audio data from it.
		   If requesting the line was successful, we are opening
		   it (important!).
		 */
		DataLine.Info	info = new DataLine.Info(TargetDataLine.class, audioFormat);
		TargetDataLine	targetDataLine = null;
		try
		{
			targetDataLine = (TargetDataLine) AudioSystem.getLine(info);
			targetDataLine.open(audioFormat);
		}
		catch (LineUnavailableException e)
		{
			e.printStackTrace();
			System.exit(1);
		}

		/* Again for simplicity, we've hardcoded the audio file
	   type, too.
		 */
		AudioFileFormat.Type	targetType = AudioFileFormat.Type.WAVE;

		/* Now, we are creating an SimpleAudioRecorder object. It
	   contains the logic of starting and stopping the
	   recording, reading audio data from the TargetDataLine
	   and writing the data to a file.
		 */
		audioRecorder = new AudioRecorder(
				targetDataLine,
				targetType,
				file);
	}
	
	public void startRecording() {
		audioRecorder.start();
		System.out.println("Recording...");
	}
	
	public void stopRecording() {
		audioRecorder.stopRecording();
		System.out.println("Recording stopped.");
	}
}
