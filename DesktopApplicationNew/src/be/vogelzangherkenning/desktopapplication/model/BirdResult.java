/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.vogelzangherkenning.desktopapplication.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.ArrayList;

/**
 *
 * @author Riccardo
 */
public class BirdResult {
    private int birdID;
    private String sampleType,birdName,family,genus,species;
    private int matches;
    private double sumPathCost;
    private double bestPathCost;
    private double averagePathCost;
    
    private JsonObject json;

    public BirdResult(JsonObject bird) {
        json = bird;
        setValues();
    }
    
    private void setValues(){
        
        birdID = json.get("BIRDID").getAsInt();
        sampleType = json.get("SAMPLETYPE").getAsString();
        birdName = json.get("BIRDNAME").getAsString();
        family = json.get("FAMILY").getAsString();
        genus =  json.get("GENUS").getAsString();
        species = json.get("SPECIES").getAsString();
        
        matches = json.get("matches").getAsInt();
        sumPathCost = json.get("sumPathCost").getAsDouble();
        bestPathCost = json.get("bestPathcost").getAsDouble();
        averagePathCost = json.get("averagePathcost").getAsDouble();
        //pathCosts.add(new Integer(json.get("bestPathCost").getAsInt()));
    }

    
    public int getMatches(){
        return matches;
    }
    
    @Override
    public String toString(){
        String ret = "";
        ret += "\t birdID : " + birdID + "\n";
        ret += "\t sampleType : " + sampleType + "\n";
        ret += "\t birdName : " + birdName + "\n";
        ret += "\t family : " + family + "\n";
        ret += "\t genus : " + genus + "\n";
        ret += "\t species : " + species + "\n";
        ret += "\t matches : " + getMatches() + "\n";
        
        return ret;
    }
    
    public int getBirdID() {
        return birdID;
    }

    public void setBirdID(int birdID) {
        this.birdID = birdID;
    }

    public String getSampleType() {
        return sampleType;
    }

    public void setSampleType(String sampleType) {
        this.sampleType = sampleType;
    }

    public String getBirdName() {
        return birdName;
    }

    public void setBirdName(String birdName) {
        this.birdName = birdName;
    }

    public String getGenus() {
        return genus;
    }

    public void setGenus(String genus) {
        this.genus = genus;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public double getSumPathCost() {
        return sumPathCost;
    }

    public double getBestPathCost() {
        return bestPathCost;
    }

    public double getAveragePathCost() {
        return averagePathCost;
    }
 

    public String getFamily() {
        return family;
    }
    
}
