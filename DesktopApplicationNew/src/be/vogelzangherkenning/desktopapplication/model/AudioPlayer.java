// Example 17-3. SoundPlayer.java
//@url http://www.onjava.com/excerpt/jenut3_ch17/examples/SoundPlayer.java
package be.vogelzangherkenning.desktopapplication.model;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Observable;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.Timer;

/**
 * This class is a Swing component that can load and play a sound clip,
 * displaying progress and controls.  The main( ) method is a test program.
 * This component can play sampled audio or MIDI files, but handles them 
 * differently. For sampled audio, time is reported in microseconds, tracked in
 * milliseconds and displayed in seconds and tenths of seconds. For midi
 * files time is reported, tracked, and displayed in MIDI "ticks".
 * This program does no transcoding, so it can only play sound files that use
 * the PCM encoding.
 */
public class AudioPlayer extends Observable {
	Clip clip;               // Contents of a sampled audio file
	boolean playing = false; // whether the sound is currently playing

	// Length and position of the sound are measured in milliseconds for 
	// sampled sounds and MIDI "ticks" for MIDI sounds
	int audioLength;         // Length of the sound.  
	int audioPosition = 0;   // Current position within the sound
	Timer timer;              // Updates slider every 100 milliseconds
	
	// Create a SoundPlayer component for the specified file.
	public AudioPlayer(File f)
			throws IOException,
			UnsupportedAudioFileException,
			LineUnavailableException,
			MidiUnavailableException,
			InvalidMidiDataException
			{

		// Getting a Clip object for a file of sampled audio data is kind
		// of cumbersome.  The following lines do what we need.
		AudioInputStream ain = AudioSystem.getAudioInputStream(f);
		try {
			DataLine.Info info =
					new DataLine.Info(Clip.class,ain.getFormat( ));
			clip = (Clip) AudioSystem.getLine(info);
			clip.open(ain);
		}
		finally { // We're done with the input stream.
			ain.close( );
		}
		// Get the clip length in microseconds and convert to milliseconds
		audioLength = (int)(clip.getMicrosecondLength( )/1000);

		// Whenever the slider value changes, first update the time label.
		// Next, if we're not already at the new position, skip to it.
//		progress.addChangeListener(new ChangeListener( ) {
//			public void stateChanged(ChangeEvent e) {
//				int value = progress.getValue( );
//				// Update the time label
//				time.setText(value/1000 + "." +
//						(value%1000)/100);
//				// If we're not already there, skip there.
//				if (value != audioPosition) skip(value);
//			}
//		});

		// This timer calls the tick( ) method 50 times a second to keep 
		// our slider in sync with the music.
		timer = new javax.swing.Timer(20, new ActionListener( ) {
			public void actionPerformed(ActionEvent e) { tick( ); }
		});
	}

	/** Start playing the sound at the current position */
	public void play( ) {
		clip.start( );
		timer.start( );
		playing = true;
	}

	/** Stop playing the sound, but retain the current position */
	public void stop( ) {
		timer.stop( );
		clip.stop( );
		playing = false;
	}

	/** Stop playing the sound and reset the position to 0 */
	public void reset( ) {
		stop( );
		clip.setMicrosecondPosition(0);
		audioPosition = 0; 
	}

	/** Skip to the specified position */
	public void skip(int position) { // Called when user drags the slider
		if (position < 0 || position > audioLength) return;
		audioPosition = position;
		clip.setMicrosecondPosition(position * 1000);
	}

	/** Return the length of the sound in ms or ticks */
	public int getLength( ) { return audioLength; }

	// An internal method that updates the progress bar.
	// The Timer object calls it 10 times a second.
	// If the sound has finished, it resets to the beginning
	void tick( ) {
		if (clip.isActive()) {
			audioPosition = (int)(clip.getMicrosecondPosition( )/1000);
			//System.out.println(audioPosition);
			
			/** Notify observer*/
			setChanged();
			float percentage = ((float) audioPosition / (float) audioLength) * 100.0f;
            notifyObservers(percentage);
            
            
            if (audioPosition >= audioLength) {
            	//System.out.println("TEST");
            	reset();
            }
		}
		else 
			reset();  
	}
}