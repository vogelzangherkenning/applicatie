
package be.vogelzangherkenning.desktopapplication.model;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

/**
 * Can copy audio
 * And get Durations of file
 */
public class AudioFunctions {
	/**
	 * Copy audio
	 * @param sourceFileName Source audio file
	 * @param destinationFileName Destination audio file
	 * @param startSecond Start second
	 * @param secondsToCopy Num seconds to copy
	 */
	public static void copyAudio(String sourceFileName, String destinationFileName, double startSecond, double secondsToCopy) {
		AudioInputStream inputStream = null;
		AudioInputStream shortenedStream = null;
		try {
			File file = new File(sourceFileName);
			AudioFileFormat fileFormat = AudioSystem.getAudioFileFormat(file);
			AudioFormat format = fileFormat.getFormat();
			inputStream = AudioSystem.getAudioInputStream(file);
			int bytesPerSecond = format.getFrameSize() * (int)format.getFrameRate();
			inputStream.skip((long)(startSecond * bytesPerSecond));
			long framesOfAudioToCopy = (long)(secondsToCopy * (int)format.getFrameRate());
			shortenedStream = new AudioInputStream(inputStream, format, framesOfAudioToCopy);
			File destinationFile = new File(destinationFileName);
			AudioSystem.write(shortenedStream, fileFormat.getType(), destinationFile);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) try { inputStream.close(); } catch (Exception e) { e.printStackTrace(); }
			if (shortenedStream != null) try { shortenedStream.close(); } catch (Exception e) { e.printStackTrace(); }
		}
	}

	/**
	 * Will return the duration of the wav file
	 * @param file Wav file
	 * @return The duration or -1 if error
	 */
	public static double getDurationOfWavInSeconds(File file)
	{   
		AudioInputStream stream = null;

		try 
		{
			stream = AudioSystem.getAudioInputStream(file);
			AudioFormat format = stream.getFormat();
			return file.length() / format.getSampleRate() / (format.getSampleSizeInBits() / 8.0) / format.getChannels();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return -1;
		}
		finally
		{
			try { stream.close(); } catch (Exception e) { e.printStackTrace(); }
		}
	}
	
	/**
	 * Get AudioInputStream
	 * @param fileName Path to wav
	 * @return The AudioInputStream or null if not found
	 */
	public static AudioInputStream getAudioInputStream(String fileName) {
		//Load file
		File file = new File(fileName);
		AudioInputStream audioInputStream = null;
		try {
			audioInputStream = AudioSystem.getAudioInputStream(new BufferedInputStream (new FileInputStream (file)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return audioInputStream;
	}
}