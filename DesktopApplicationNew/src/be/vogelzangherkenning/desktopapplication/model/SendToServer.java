package be.vogelzangherkenning.desktopapplication.model;

import be.vogelzangherkenning.desktopapplication.view.result.ResultFrame;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Class to send file to server and return json response
 */
public class SendToServer {
	private static final String AUDIO_ENDPOINT = "/audio";
	
	/**
	 * Send to server and return json
	 * @param audioFile Name of audioFile
	 * @param server Name of server (in send we will add endpoint, so without / at the end)
	 * @return Json responsr or null if something went wrong
	 */
	public static String send(String audioFile, String server) {
		FileInputStream fileInputStream = null;
		File f = new File(audioFile);
		byte[] bFile = new byte[(int) f.length()];

		try {
			//convert file into array of bytes
			fileInputStream = new FileInputStream(f);
			fileInputStream.read(bFile);
			fileInputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			String request = getRequestURL(server);
			URL url = new URL(request);
			
			/** Make connection*/
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setInstanceFollowRedirects(false);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "audio/x-wav");
			connection.setUseCaches(false);

			/** Write bytes to outputstream*/
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.write(bFile);
			wr.flush();
			wr.close();
			
			/** Read answer*/
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
			StringBuilder builder = new StringBuilder();
			
			/** Read lines answer*/
			String aux = "";
			while ((aux = in.readLine()) != null) {
				builder.append(aux);
			}

			/** JSON response*/
			String json = builder.toString();
			return json;
                        
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	/**
	 * Return the server request url (SERVER + AUDIO_ENDPOINT)
	 * @param server
	 * @return
	 */
	private static String getRequestURL(String server) {
		return server + AUDIO_ENDPOINT;
	}
}
