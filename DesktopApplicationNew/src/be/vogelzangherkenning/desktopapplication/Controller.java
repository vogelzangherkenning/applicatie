package be.vogelzangherkenning.desktopapplication;

import java.io.File;

import javax.swing.JOptionPane;

import be.vogelzangherkenning.desktopapplication.model.AudioFunctions;
import be.vogelzangherkenning.desktopapplication.model.AudioPlayer;
import be.vogelzangherkenning.desktopapplication.model.MyAudioRecorder;
import be.vogelzangherkenning.desktopapplication.model.SendToServer;
import be.vogelzangherkenning.desktopapplication.model.ServerResult;
import be.vogelzangherkenning.desktopapplication.view.result.ResultFrame;
import be.vogelzangherkenning.desktopapplication.view.visualize.WavePanelContainer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

/**
 * Model class
 */
public class Controller {
	public static final String DEFAULT_FILE = "myrecording.wav";
	private static final String RECORDED_FILE = DEFAULT_FILE;
	private static final String CUTTED_FILE = "cutted.wav";
        private static final String TEMP_FILE = "temp.wav";

	//Singleton (Probably not the best design decision :-) )
	//Is also controller
	private static Controller model = null;

	private String currentFile = DEFAULT_FILE;
	private String beforeCuttedFile = DEFAULT_FILE;

	private AudioPlayer audioPlayer = null;
	private MyAudioRecorder audioRecorder = null;

	//The view
	private View view;

	private Controller() {
		//Lege constructor
	}

	public static Controller getInstance() {
		if (model == null)
			model = new Controller();

		return model;
	}

	public void startRecording() {
		audioRecorder = new MyAudioRecorder(RECORDED_FILE);
		audioRecorder.startRecording();
	}

	public void stopRecording() {
		if (audioRecorder != null) {
			audioRecorder.stopRecording();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ie) {
				System.err.println(ie.getMessage());
			}
			loadFile(RECORDED_FILE);
		}
	}

	public void loadFile(String filePath) {
		//Set currentfile
		this.currentFile = filePath;

		//Load new wav
		view.getAudioContainer().setAudioToDisplay(AudioFunctions.getAudioInputStream(currentFile));
	}


	public void startPlaying() {
                cutBeforePlay();
		System.out.println("StartPlaying:" + new File(TEMP_FILE).getAbsolutePath());
		try {
			audioPlayer = new AudioPlayer(new File(TEMP_FILE));
			audioPlayer.addObserver(view.getAudioContainer().getWavePanel());
			audioPlayer.play();
		} catch (Exception e) {
			e.printStackTrace();
			audioPlayer = null;
		}
	}

	public void stopPlaying() {
		if (audioPlayer != null)
			audioPlayer.stop();
	}

        private void cutBeforePlay(){
            //Get start and stop 
            int start = view.getAudioContainer().getSliderLowValue();
            int stop = view.getAudioContainer().getSliderHighValue();
            String fileToCut = currentFile;
            double length = AudioFunctions.getDurationOfWavInSeconds(new File(fileToCut));
            double startS = length * (start/100.0f);
            double stopS =  length*((stop-start)/100.0f);
            if (stopS - startS < 1) {
                view.showWarning("Can not cut (is to small)");
		//Reload current file so slider is back 0 --> 100
		loadFile(currentFile);
		return;
            }
            AudioFunctions.copyAudio(fileToCut,TEMP_FILE, startS, stopS);
        }
	public void cut() {
		//Get start and stop 
		int start = view.getAudioContainer().getSliderLowValue();
		int stop = view.getAudioContainer().getSliderHighValue();

		beforeCuttedFile = currentFile;
		double length = AudioFunctions.getDurationOfWavInSeconds(new File(currentFile));
		double startS = length * (start/100.0f);
		double stopS =  length*((stop-start)/100.0f);

		System.out.println("startS=" + startS);
		System.out.println("stopS=" + stopS);
		if (stopS - startS < 1) {
			view.showWarning("Can not cut (is to small)");
			//Reload current file so slider is back 0 --> 100
			loadFile(currentFile);
			return;
		}
		AudioFunctions.copyAudio(currentFile,CUTTED_FILE, startS, stopS);
		loadFile(CUTTED_FILE);
		/*try {
			CheapWAV wav = new CheapWAV();
			wav.ReadFile(new File(currentFile));
			int startFrame = wav.secondsToFrames(startS);
			int endFrame = wav.secondsToFrames(stopS);
			wav.WriteFile(new File(CUTTED_FILE), 
					startFrame, 
					endFrame - startFrame);
			System.out.println("copyAudio done");
			loadFile(CUTTED_FILE);
			System.out.println("After loadfile");
		} catch (Exception e) {
			e.printStackTrace();
		}*/
	}

	public void undoCut() {
		loadFile(beforeCuttedFile);
	}

	public void send(String server) {
		String json = SendToServer.send(currentFile,server);                
		//If there was response
		if (json != null) {
			//Convert string to readable json
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			JsonObject o = gson.fromJson(json,JsonObject.class);
			json = gson.toJson(o);
			ServerResult res = new ServerResult(o);
			//TODO show json response
			new ResultFrame(res);
			System.out.println(json);
		}
	}

	public void setView(View view) {
		this.view = view;
	}

	public String getCurrentFile() {
		return currentFile;
	}
}
