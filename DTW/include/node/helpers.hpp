#include "../prdat/core/matrix.hpp"

#include <node/node.h>
#include <v8.h>
#include <vector>

using namespace v8;

/**
 * Convert v8 array handle to vector
 */
std::vector<double> arrayToDoubleVector(const Handle<Array>& arr ) {
	std::vector<double> vector;

	for( uint32_t i=0; i<arr->Length(); i++ ) {
		vector.push_back( arr->Get(i)->NumberValue() );
	}

	return vector;
}

/**
 * Convert vector to v8 array handle
 */
Handle<Array> doubleVectorToArray(const std::vector<double>& vector ) {
	Handle<Array> arr = Array::New(vector.size());
	uint32_t pos = 0;
	std::for_each(vector.begin(),
								vector.end(),
								[&](double x) {
									arr->Set(pos++, Number::New(x));
								});

  return arr;
}

/**
 * Convert matrix to v8 array handle
 */
Handle<Array> matrixToArray(const core::matrix<double>& mat)
{
	Handle<Array> arr = Array::New(mat.rows() * mat.columns());
	for(std::size_t r = 0; r < mat.rows(); ++r) {
		for(std::size_t c = 0; c < mat.columns(); ++c) {
			arr->Set((r * mat.columns()) + c, Number::New(mat[r][c]));
		}
	}
	return arr;
}

/**
 * Convert v8 array handle to matrix
 */
core::matrix<double> arrayToMatrix(const Handle<Array>& arr, int rows, int cols)
{
	core::matrix<double> mat(rows, cols);
	for(std::size_t r = 0; r < rows; ++r) {
		for(std::size_t c = 0; c < cols; ++c) {
			mat[r][c] = arr->Get((r * cols) + c)->NumberValue();
		}
	}
	return mat;
}
