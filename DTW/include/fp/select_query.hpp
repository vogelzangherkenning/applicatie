/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef _SELECT_QUERY_HPP
#define _SELECT_QUERY_HPP

#include "impl/select_query_impl.hpp"

#include "is_query.hpp"
#include "record.hpp"

#include "mpl/type_sequence.hpp"

#include <string>       // for std::string, std::to_string

namespace fp
{

struct extract_columns_t { };
constexpr static extract_columns_t extract_columns { };

template<typename...>
struct select_query;

template<typename... TColumns>
struct is_select_query<select_query<TColumns...> >
: mpl::all_<is_column<TColumns>...> { };

template<typename... TColumns>
struct is_query<select_query<TColumns...> >
: is_select_query<select_query<TColumns...>> { };

template<typename... TColumns>
struct select_query
{
public:
  template<typename TRecord>
  struct result_of : TRecord::template rebind<TColumns...>
  { };

public:
  constexpr select_query() noexcept = default;

public:
  template<
    typename TRecord,
    typename = mpl::enable_if_t<is_record<TRecord>>
  >
  friend Invoke<result_of<TRecord>> select(const TRecord& rec,
                                           const select_query& self)
  {
    return { fp::get(rec, TColumns())... };
  }

  template<
    typename TContainer,
    typename TRecord = typename TContainer::value_type,
    typename = mpl::enable_if_t<is_record<TRecord>>
  >
  friend typename TContainer::template rebind<Invoke<result_of<TRecord>>>::type
  select(const TContainer& recs, const select_query& self)
  {
    using Result = Invoke<result_of<TRecord>>;
    using TReturnContainer = typename TContainer::template rebind<Result>::type;
    TReturnContainer ret;
    ret.reserve(recs.size());
    for(const TRecord& cur : recs) {
      ret.push_back(select(cur, self));
    }
    return ret;
  }

  template<
    typename TContainer,
    typename TRecord = typename TContainer::value_type,
    typename = mpl::enable_if_t<is_record<TRecord>>
  >
  friend typename TContainer::template rebind<Invoke<result_of<TRecord>>>::type
  query(const TContainer& recs, const select_query& self)
  {
    return select(recs, self);
  }

  friend std::string to_string(const select_query& self)
  {
    return impl::select_query_impl<TColumns...>::build_select_query();
  }

};

template<
  typename... TColumns,
  typename = mpl::enable_if_t<mpl::all_<is_column<TColumns>...>>
>
constexpr inline select_query<TColumns...> select(TColumns...)
{
  return { };
}

template<
  template<typename...> class TRecord,
  typename... TColumns,
  typename = mpl::enable_if_t<mpl::all_<is_column<TColumns>...>>
>
constexpr inline select_query<TColumns...> select(extract_columns_t, TRecord<TColumns...>)
{
  return { };
}

} // namespace fp

#endif
