/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef _SKIP_VALUES_IMPL_HPP
#define _SKIP_VALUES_IMPL_HPP

#include "../mpl/identity.hpp"
#include "../mpl/integer_sequence.hpp"

namespace fp
{

namespace impl
{

template<int, bool, int...>
struct skip_values_helper;

template<int, int...>
struct skip_values_impl;

template<int C, int H, int... T>
struct skip_values_impl<C, H, T...>
: skip_values_impl<(C - 1), T...> { };

template<int H, int... T>
struct skip_values_impl<0, H, T...>
: mpl::identity<mpl::integer_sequence<int, H, T...>> { };

template<>
struct skip_values_impl<0>
: mpl::identity<mpl::integer_sequence<int>> { };

template<int C, bool S, int... I>
struct skip_values_helper
: skip_values_impl<C, I...> { };

template<int C, int... I>
struct skip_values_helper<C, true, I...>
: mpl::identity<mpl::integer_sequence<int>> { };

} // namespace impl

}

#endif
