/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef FUNCTOR_IMPL_HPP
#define FUNCTOR_IMPL_HPP

#include "../call_with.hpp"   // for fp::call_function

#include <tuple>              // for std::tuple

namespace fp
{

namespace impl
{

template<typename Fn>
struct functor
{
public:
  functor(Fn fn)
  : fn_(fn)
  { }

  void run() const
  {
    (*fn_)();
  }

public:
  friend void run(const functor& self)
  {
    self.run();
  }

private:
  Fn fn_;
};

template<typename Fn, typename... Arg>
struct functor_with_args
{
public:
  functor_with_args(Fn fn, Arg... arg)
  : fn_(fn)
  , m_arg(arg...)
  { }

  void run() const
  {
    call_function(fn_, m_arg);
  }

public:
  friend void run(const functor_with_args& self)
  {
    self.run();
  }

private:
  Fn fn_;
  std::tuple<Arg...> m_arg;
};

template<typename C>
struct member_functor
{
public:
  member_functor(void(C::*fn)(), C* obj)
  : fn_(fn)
  , m_obj(obj)
  { }

  void run() const
  {
    (m_obj->*fn_)();
  }

public:
  friend void run(const member_functor& self)
  {
    self.run();
  }

private:
  void(C::*fn_)();
  C* m_obj;
};

template<typename C, typename... Arg>
struct member_functor_with_args
{
public:
  member_functor_with_args(void(C::*fn)(Arg...), C* obj, Arg... arg)
  : fn_(fn)
  , m_obj(obj)
  , m_arg(arg...)
  { }

  void run() const
  {
    call_function(fn_, m_obj, m_arg);
  }

public:
  friend void run(const member_functor_with_args& self)
  {
    self.run();
  }

private:
  void(C::*fn_)(Arg...);
  C* m_obj;
  std::tuple<Arg...> m_arg;
};

} // namespace impl

}

#endif
