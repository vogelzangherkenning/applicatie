/* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at http://mozilla.org/MPL/2.0/.*/

#ifndef MYSQL_BASIC_ENGINE_HPP
#define MYSQL_BASIC_ENGINE_HPP

#include "impl/basic_engine_impl.hpp"

#include "basic_context.hpp"
#include "basic_result.hpp"
#include "basic_row.hpp"
#include "../assert.hpp"
#include "../core/non_copyable.hpp"
#include "../db_engine.hpp"     // for fp::is_engine
#include "../forward.hpp"       // for fix::forward
#include "../is_query.hpp"      // for fp::is_query
#include "../record.hpp"
#include "../type_traits.hpp"   // for fp::EnableIf, fp::DisableIf, fp::Invoke, fp::Unqualified

#include <algorithm>
#include <iterator>             // for std::back_inserter
#include <memory>               // for std::shared_ptr
#include <string>               // for std::string, std::to_string
#include <utility>              // for std::swap
#include <vector>               // for std::vector

#include <mysql/mysql.h>        // for MYSQL, MYSQL_RES

namespace fp
{

namespace mysql
{

class basic_engine : non_copyable
{
public:
  using default_record_type = record<>;

public:
  basic_engine(const char* host, const char* user, const char* pwd)
  : _context(basic_context::create()) {
    if (_context) {
      _context->real_connect(host, user, pwd, nullptr);
      FP_ASSERT(*_context, "Unable to connect to MySQL server");
    }
  }

  basic_engine(const char* host, const char* user, const char* pwd, const char* db)
  : _context(basic_context::create()) {
    if (_context) {
      _context->real_connect(host, user, pwd, db);
      FP_ASSERT(*_context, "Unable to connect to MySQL server");
    }
  }

  basic_engine(const basic_engine&) = default;
  basic_engine(basic_engine&&) = default;

  friend void swap(basic_engine& l, basic_engine& r)
  {
    using std::swap;
    swap(l._context, r._context);
  }

  std::string errorstr() const
  {
    return _context->errorstr();
  }

  unsigned int error() const
  {
    return _context->error();
  }

  template<
    typename TQuery,
    typename TRecord = Invoke<typename Unqualified<TQuery>::template result_of<default_record_type>>,
    typename Container = std::vector<TRecord>,
    typename = mpl::enable_if_t<mpl::all_<is_query<Unqualified<TQuery>>, is_record<TRecord>>>
  >
  Container query(TQuery&& q)
  {
    using std::to_string;
    using std::begin;
    using std::end;

    const std::string qry = to_string(fix::forward<TQuery>(q));
    if(0 != _context->query(qry)) {
      throw std::runtime_error("Query failed: " + qry);
    }

    basic_result res(*_context);
    if(res) {
      Container ret;
      ret.reserve(res.rows());
      std::transform(
        begin(res),
        end(res),
        std::back_inserter(ret),
        [](const basic_row& r) {
          return impl::make_record<TRecord>::make(r);
        }
      );
      return ret;
    }

    throw std::runtime_error("No valid context available");
  }

  template<
    typename TQuery,
    typename TRecord = Invoke<typename Unqualified<TQuery>::template result_of<default_record_type>>,
    typename = mpl::disable_if_t<mpl::all_<is_query<Unqualified<TQuery>>, is_record<TRecord>>>
  >
  unsigned long long query(TQuery&& q)
  {
    using std::to_string;

    const std::string qry = to_string(fix::forward<TQuery>(q));
    if (0 == _context->query(qry)) {
      return _context->affected_rows();
    } else {
      return -1;
    }
  }

protected:
  std::unique_ptr<basic_context> _context;
};

} // namespace mysql

namespace detail
{

template<>
struct is_engine<mysql::basic_engine> : mpl::true_ { };

} // namespace detail

}

#endif
