/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MYSQL_BASIC_CONTEXT_HPP
#define MYSQL_BASIC_CONTEXT_HPP

#include "../unique_ptr.hpp"
#include "../core/non_copyable.hpp"

#include <memory>               // for std::unique_ptr
#include <utility>              // for std::swap

#include <mysql/mysql.h>        // for MYSQL

namespace fp
{

namespace mysql
{

class basic_context;
class basic_engine;
class basic_result;

class basic_context : non_copyable
{
public:
  using this_type = basic_context;
  using native_handle = ::MYSQL;

public:
  basic_context()
  : _context(::mysql_init(nullptr), &::mysql_close)
  { }

  basic_context(basic_context&& ctx)
  : _context(nullptr, &::mysql_close)
  { swap(*this, ctx); }

  friend void swap(basic_context& l, basic_context& r)
  {
    using std::swap;
    swap(l._context, r._context);
  }

  static std::unique_ptr<basic_context> create()
  {
    native_handle* const ctx = ::mysql_init(nullptr);
    return (ctx) ? std::unique_ptr<basic_context>(new basic_context(ctx))
                 : std::unique_ptr<basic_context>(nullptr);
  }

  bool real_connect(const char* host, const char* name, const char* pass, const char* db)
  {
    native_handle* ctx = ::mysql_real_connect(_context.get(), host, name, pass, db, 0, 0, 0);
    if (_context.get() != ctx) {
      _context.reset(ctx);
    }
    return (ctx != NULL);
  }

  explicit operator bool() const
  {
    return static_cast<bool>(_context);
  }

  operator native_handle*() const
  {
    return _context.get();
  }

  int query(const std::string& qry)
  {
    return ::mysql_query(_context.get(), qry.c_str());
  }

  int affected_rows() const
  {
    return ::mysql_affected_rows(_context.get());
  }

  unsigned int error() const
  {
    return ::mysql_errno(_context.get());
  }

  std::string errorstr() const
  {
    return ::mysql_error(_context.get());
  }

protected:
  friend class basic_engine;
  friend class basic_result;

  explicit basic_context(native_handle* const ctx)
  : _context(ctx, &::mysql_close)
  { }

  inline native_handle* handle() const
  {
    return _context.get();
  }

  basic_context& operator=(native_handle* const ctx)
  {
    if (_context.get() != ctx) {
      _context.reset(ctx);
    }
    return *this;
  }

  std::unique_ptr<native_handle, decltype(&::mysql_close)> _context;
};

} // namespace mysql

}

#endif
