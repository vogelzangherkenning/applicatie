/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef _UNIQUE_TYPES_HPP
#define _UNIQUE_TYPES_HPP

#include "is_contained_type.hpp"
 #include "type_traits.hpp"

namespace fp
{

template<typename...>
struct unique_types;

namespace impl
{

template<typename, typename>
struct unique_types_impl;

template<bool, typename, typename>
struct unique_types_helper;

template<typename... TLeft, typename H, typename... T>
struct unique_types_impl<mpl::type_sequence<TLeft...>,
                         mpl::type_sequence<H, T...> >
: mpl::identity<Invoke<unique_types_helper<is_contained_type<H, TLeft...>::value,
                                           mpl::type_sequence<TLeft...>,
                                           mpl::type_sequence<H, T...>>>>
{ };

template<typename... TLeft>
struct unique_types_impl<mpl::type_sequence<TLeft...>, mpl::type_sequence<> >
: mpl::identity<mpl::type_sequence<TLeft...> > { };

template<typename... TLeft, typename TRightHead, typename... TRightTail>
struct unique_types_helper<false,
                           mpl::type_sequence<TLeft...>,
                           mpl::type_sequence<TRightHead, TRightTail...>>
: mpl::identity<Invoke<unique_types_impl<mpl::type_sequence<TLeft..., TRightHead>,
                                         mpl::type_sequence<TRightTail...>>>>
{ };

template<typename... TLeft, typename TRightHead, typename... TRightTail>
struct unique_types_helper<true,
                           mpl::type_sequence<TLeft...>,
                           mpl::type_sequence<TRightHead, TRightTail...>>
: mpl::identity<Invoke<unique_types_impl<mpl::type_sequence<TLeft...>,
                                         mpl::type_sequence<TRightTail...>>>>
{ };

} // namespace impl

template<typename H, typename... T>
struct unique_types<H, T...>
: impl::unique_types_impl<mpl::type_sequence<H>, mpl::type_sequence<T...>> { };

}

#endif
