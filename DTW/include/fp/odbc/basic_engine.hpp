#ifndef ODBC_BASIC_ENGINE_HPP_
#define ODBC_BASIC_ENGINE_HPP_

#include "basic_connection.hpp"
#include "basic_result.hpp"
#include "basic_row.hpp"
#include "helpers.hpp"
#include "../core/non_copyable.hpp"
#include "../assert.hpp"
#include "../db_engine.hpp"     // for fp::is_engine
#include "../forward.hpp"       // for fix::forward
#include "../is_query.hpp"      // for fp::is_query
#include "../record.hpp"
#include "../type_traits.hpp"   // for fp::EnableIf, fp::DisableIf, fp::Invoke, fp::Unqualified

#include <memory>

namespace fp
{

namespace odbc
{

class basic_engine : non_copyable
{
public:
  using this_type = basic_engine;
  using default_record_type = record<>;

public:
  basic_engine(const char* host, const char* user, const char* pwd)
  : connection_(basic_connection::create(host, user, pwd))
  { }

  ~basic_engine()
  { }

  explicit operator bool() const
  {
    FP_ASSERT(connection_, "Connection is NULL");
    return static_cast<bool>(*connection_);
  }

  std::string errorstr() const
  {
    FP_ASSERT(connection_, "Connection is NULL");
    return connection_->errorstr();
  }

  unsigned int error() const
  {
    FP_ASSERT(connection_, "Connection is NULL");
    return connection_->error();
  }

  template<
    typename TQuery,
    typename TRecord = Invoke<typename Unqualified<TQuery>::template result_of<default_record_type>>,
    typename Container = std::vector<TRecord>,
    typename = mpl::enable_if_t<mpl::all_<is_query<Unqualified<TQuery>>, is_record<TRecord>>>
  >
  Container query(TQuery&& q)
  {
    FP_ASSERT(connection_, "Connection is NULL");
    using std::to_string;
    using std::begin;
    using std::end;

    const std::string qry = to_string(fix::forward<TQuery>(q));
    SQLHSTMT stmt = connection_->create_statement();
    SQLRETURN res = SQLExecDirect(stmt, (SQLCHAR*)qry.c_str(), qry.size());
    odbc_report("odbc::basic_engine::query : SQLExecDirect", res);

    if(odbc_success(res)) {
      basic_result<TRecord> res(stmt);
      Container ret;
      /* TODO: make res.rows() return the actual number of rows */
      //ret.reserve(res.rows());
      std::copy(begin(res), end(res), std::back_inserter(ret));
      return ret;
    }

    if(res == SQL_INVALID_HANDLE) {
      throw std::runtime_error("Invalid handle");
    } else {
      throw std::runtime_error(connection_->errorstr());
    }
  }

  template<
    typename TQuery,
    typename TRecord = Invoke<typename Unqualified<TQuery>::template result_of<default_record_type>>,
    typename = mpl::disable_if_t<mpl::all_<is_query<Unqualified<TQuery>>, is_record<TRecord>>>
  >
  unsigned long long query(TQuery&& q)
  {
    FP_ASSERT(connection_, "Connection is NULL");
    const std::string qry = to_string(fix::forward<TQuery>(q));
    SQLHSTMT stmt = connection_->create_statement();
    SQLRETURN res = SQLExecDirect(stmt, (SQLCHAR*)qry.c_str(), qry.size());
    odbc_report("odbc::basic_engine::query : SQLExecDirect", res);

    if(odbc_success(res)) {
      basic_result<TRecord> res(stmt);
      return res.rows();
    }

    if(res == SQL_INVALID_HANDLE) {
      throw std::runtime_error("Invalid handle");
    } else {
      throw std::runtime_error(connection_->errorstr());
    }
  }

public:
  friend void swap(basic_engine& l, basic_engine& r)
  {
    using std::swap;
    swap(l.connection_, r.connection_);
  }

private:
  std::unique_ptr<basic_connection> connection_;
};

} // namespace odbc

namespace detail
{

template<>
struct is_engine<odbc::basic_engine> : mpl::true_ { };

} // namespace detail

}

#endif
