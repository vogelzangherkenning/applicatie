#ifndef ODBC_HELPERS_HPP_
#define ODBC_HELPERS_HPP_

#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>

namespace fp
{

namespace odbc
{

inline bool odbc_success(SQLRETURN res)
{
  switch(res)
  {
    case SQL_SUCCESS:
    case SQL_SUCCESS_WITH_INFO:
      return true;
    default:
      return false;
  }
}

inline void odbc_report(const char* fn, SQLRETURN res)
{
  if(!odbc_success(res)) {
    std::cout << fn << " returned ODBC error" << std::endl;
  }
}

} // namespace odbc

}

#endif
