/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef _ODBC_BASIC_RESULT_HPP
#define _ODBC_BASIC_RESULT_HPP

#include "basic_row.hpp"
#include "basic_context.hpp"
#include "basic_result_iterator.hpp"
#include "helpers.hpp"
#include "../core/non_copyable.hpp"

#include <cstddef>              // for std::size_t
#include <memory>               // for std::unique_ptr
#include <stdexcept>            // for std::runtime_error
#include <utility>              // for std::swap

namespace fp
{

namespace odbc
{

template<typename TRecord>
class basic_result : non_copyable
{
public:
  using this_type = basic_result;
  using size_type = std::size_t;
  using record_type = TRecord;
  using iterator = result_iterator<record_type*>;
  using const_iterator = result_iterator<const record_type*>;

public:
  basic_result(SQLHSTMT stmt)
  : stmt_(stmt)
  { }

  basic_result(basic_result&& res)
  { swap(*this, res); }

  friend void swap(basic_result& l, basic_result& r)
  {
    using std::swap;
    swap(l.stmt_, r.stmt_);
  }

  explicit operator bool() const
  {
    return (stmt_ != SQL_NULL_HSTMT);
  }

  unsigned int error() const
  {
    SQLCHAR sqlstate[6];
    SQLINTEGER err = 0;
    SQLCHAR message[SQL_MAX_MESSAGE_LENGTH];
    SQLSMALLINT message_len = 0;
    SQLRETURN res = SQLGetDiagRec(SQL_HANDLE_STMT,
                                  stmt_,
                                  1,
                                  sqlstate,
                                  &err,
                                  message,
                                  sizeof(message),
                                  &message_len);
    odbc_report("odbc::basic_connection::error : SQLGetDiagRec", res);

    if(odbc_success(res)) {
      return err;
    }

    return 0;
  }

  std::string errorstr() const
  {
    SQLCHAR sqlstate[6];
    SQLINTEGER err = 0;
    SQLCHAR message[SQL_MAX_MESSAGE_LENGTH];
    SQLSMALLINT message_len = 0;
    SQLRETURN res = SQLGetDiagRec(SQL_HANDLE_STMT,
                                  stmt_,
                                  1,
                                  sqlstate,
                                  &err,
                                  message,
                                  sizeof(message),
                                  &message_len);
    odbc_report("odbc::basic_connection::errorstr : SQLGetDiagRec", res);

    if(odbc_success(res)) {
      return std::string(&message[0], &message[message_len]);
    }

    return "";
  }

  size_type rows() const
  {
    SQLLEN count = 0;
    SQLRETURN res = SQLRowCount(stmt_, &count);
    odbc_report("SQLRowCount", res);
    if(odbc_success(res)) {
      return count;
    }
    return 0;
  }

  size_type cols() const
  {
    SQLSMALLINT count = 0;
    SQLRETURN res = SQLNumResultCols(stmt_, &count);
    odbc_report("SQLNumResultCols", res);
    if(odbc_success(res)) {
      return count;
    }
    return 0;
  }

  iterator begin()
  {
    return iterator(stmt_);
  }

  iterator end()
  {
    return iterator();
  }

public:
  friend iterator begin(basic_result& self)
  {
    return self.begin();
  }

  friend iterator end(basic_result& self)
  {
    return self.end();
  }

private:
  SQLHSTMT stmt_;
};

} // namespace odbc

}

#endif
