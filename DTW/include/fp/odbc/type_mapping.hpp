#ifndef ODBC_TYPE_MAPPING_HPP_
#define ODBC_TYPE_MAPPING_HPP_

namespace fp
{

namespace odbc
{

template<typename T>
struct to_odbc_type
{
  using type = T;
};
template<typename T, std::size_t N>
struct to_odbc_type<T[N]>
{
  using type = T[N];
};

template<typename T>
struct from_odbc_type
{
  using type = T;
};
template<typename T, std::size_t N>
struct from_odbc_type<T[N]>
{
  using type = T[N];
};

#define CREATE_MAPPING(local, remote) \
  template<> \
  struct to_odbc_type<local> \
  { \
    using type = remote; \
  }; \
  template<> \
  struct from_odbc_type<remote> \
  { \
    using type = local; \
  }

#define CREATE_ARRAY_MAPPING(local, remote) \
  template<std::size_t N> \
  struct to_odbc_type<local[N]> \
  { \
    using type = remote[N]; \
  }; \
  template<std::size_t N> \
  struct from_odbc_type<remote[N]> \
  { \
    using type = local[N]; \
  }

CREATE_MAPPING(void, VOID);
CREATE_MAPPING(void*, SQLHANDLE);

CREATE_MAPPING(char, SQLCHAR);

CREATE_MAPPING(short, SQLSMALLINT);
CREATE_MAPPING(unsigned short, SQLUSMALLINT);
CREATE_MAPPING(int, SQLINTEGER);
CREATE_MAPPING(unsigned int, SQLUINTEGER);
CREATE_MAPPING(long, SQLLEN);
CREATE_MAPPING(unsigned long, SQLULEN);
CREATE_MAPPING(double, SQLDOUBLE);

CREATE_ARRAY_MAPPING(char, SQLCHAR);

#undef CREATE_MAPPING
#undef CREATE_ARRAY_MAPPING

} // namespace odbc

}

#endif
