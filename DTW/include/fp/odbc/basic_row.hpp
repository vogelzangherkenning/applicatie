/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef _ODBC_BASIC_ROW_HPP
#define _ODBC_BASIC_ROW_HPP

#include "type_mapping.hpp"
#include "../type_traits.hpp"

#include <cstddef>      // for std::size_t
#include <tuple>        // for std::tuple
#include <utility>      // for std::swap

namespace fp
{

namespace odbc
{

template<typename... T>
struct basic_row
{
public:
  using this_type = basic_row;
  using size_type = std::size_t;

protected:
  std::tuple<typename to_odbc_type<T>::type...> data_;

public:
  constexpr basic_row() noexcept
  : data_()
  { }

  basic_row(basic_row&& o)
  : data_(std::move(o.data_))
  { }

  friend void swap(basic_row& l, basic_row& r) noexcept
  {
    using std::swap;
    swap(l.data_, r.data_);
  }

  constexpr size_type cols() const
  {
    return sizeof...(T);
  }

  template<
    std::size_t Idx,
    typename Return = typename from_odbc_type<NthTypeOf<Idx, T...>>::type
  >
  friend constexpr Return get(const basic_row& self)
  {
    return static_cast<Return>(std::get<Idx>(self.data_));
  }
};

} // namespace odbc

}

#endif
