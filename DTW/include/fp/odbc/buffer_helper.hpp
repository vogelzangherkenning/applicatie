#ifndef ODBC_BUFFER_HELPER_HPP_
#define ODBC_BUFFER_HELPER_HPP_

#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>

namespace fp
{

namespace odbc
{

template<std::size_t Idx, typename T>
void bind_buffer(SQLHSTMT stmt, T& x)
{
  std::cout << __PRETTY_FUNCTION__ << ": binding unspecified buffer" << std::endl;
  SQLBindCol(stmt, Idx, SQL_C_CHAR, &x, sizeof(x), NULL);
}

template<std::size_t Idx>
void bind_buffer(SQLHSTMT stmt, std::string& x)
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  x.resize(1024);
  SQLBindCol(stmt, Idx, SQL_C_CHAR, (SQLPOINTER)x.c_str(), x.size(), NULL);
}

template<std::size_t Idx>
void bind_buffer(SQLHSTMT stmt, char& x)
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  SQLBindCol(stmt, Idx, SQL_C_CHAR, &x, sizeof(x), NULL);
}

template<std::size_t Idx>
void bind_buffer(SQLHSTMT stmt, short& x)
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  SQLBindCol(stmt, Idx, SQL_C_SHORT, &x, sizeof(x), NULL);
}

template<std::size_t Idx>
void bind_buffer(SQLHSTMT stmt, int& x)
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  SQLBindCol(stmt, Idx, SQL_C_LONG, &x, sizeof(x), NULL);
}

template<std::size_t Idx>
void bind_buffer(SQLHSTMT stmt, float& x)
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  SQLBindCol(stmt, Idx, SQL_C_FLOAT, &x, sizeof(x), NULL);
}

template<std::size_t Idx>
void bind_buffer(SQLHSTMT stmt, double& x)
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  SQLBindCol(stmt, Idx, SQL_C_DOUBLE, &x, sizeof(x), NULL);
}

template<std::size_t Idx, typename T, std::size_t N>
void bind_buffer(SQLHSTMT stmt, T(&x)[N])
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  SQLBindCol(stmt, Idx, SQL_C_CHAR, &x, sizeof(x) * N, NULL);
}

} // namespace odbc

}

#endif
