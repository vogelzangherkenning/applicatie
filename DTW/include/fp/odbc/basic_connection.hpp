#ifndef ODBC_BASIC_CONNECTION_HPP_
#define ODBC_BASIC_CONNECTION_HPP_

#include "basic_context.hpp"
#include "basic_result.hpp"
#include "helpers.hpp"
#include "../core/non_copyable.hpp"

#include <cstdlib>
#include <cstdio>
#include <stdexcept>

#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>

namespace fp
{

namespace odbc
{

class basic_connection : non_copyable
{
public:
  typedef basic_connection this_type;
  typedef SQLHDBC native_handle;

  constexpr static native_handle kInvalidHandle = SQL_NULL_HDBC;

protected:
  basic_connection(native_handle conn)
  : connection_(conn)
  { }

public:
  ~basic_connection()
  {
    SQLDisconnect(connection_);
    SQLFreeHandle(SQL_HANDLE_DBC, connection_);
  }

  explicit operator bool() const
  {
    return (kInvalidHandle != connection_);
  }

  operator native_handle() const
  {
    return connection_;
  }

  SQLHSTMT create_statement()
  {
    SQLHSTMT stmt;
    SQLRETURN res = SQLAllocHandle(SQL_HANDLE_STMT, connection_, &stmt);
    odbc_report("odbc::basic_connection::create_statement : SQLAllocHandle", res);
    return (odbc_success(res)) ? stmt : SQL_NULL_HSTMT;
  }

  unsigned int error() const
  {
    SQLCHAR sqlstate[6];
    SQLINTEGER err = 0;
    SQLCHAR message[SQL_MAX_MESSAGE_LENGTH];
    SQLSMALLINT message_len = 0;
    SQLRETURN res = SQLGetDiagRec(SQL_HANDLE_DBC,
                                  connection_,
                                  1,
                                  sqlstate,
                                  &err,
                                  message,
                                  sizeof(message),
                                  &message_len);
    odbc_report("odbc::basic_connection::error : SQLGetDiagRec", res);
    switch(res)
    {
      case SQL_SUCCESS:
      case SQL_SUCCESS_WITH_INFO:
        return err;
      case SQL_NO_DATA:
        return 0;
      default:
        return -1;
    }
  }

  std::string errorstr() const
  {
    SQLCHAR sqlstate[6];
    SQLINTEGER err = 0;
    SQLCHAR message[SQL_MAX_MESSAGE_LENGTH];
    SQLSMALLINT message_len = 0;
    SQLRETURN res = SQLGetDiagRec(SQL_HANDLE_DBC,
                                  connection_,
                                  1,
                                  sqlstate,
                                  &err,
                                  message,
                                  sizeof(message),
                                  &message_len);
    odbc_report("odbc::basic_connection::errorstr : SQLGetDiagRec", res);
    switch(res)
    {
      case SQL_SUCCESS:
      case SQL_SUCCESS_WITH_INFO:
        return std::string(&message[0], &message[message_len]);
      case SQL_NO_DATA:
        /* TODO: maybe return something specific? */
      default:
        return "";
    }
  }

public:
  static std::unique_ptr<basic_connection> create(const char* host,
                                                  const char* user,
                                                  const char* pwd)
  {
    std::unique_ptr<basic_context> ctx = basic_context::create();
    if(ctx)
    {
      native_handle conn = kInvalidHandle;
      SQLRETURN res;
      res = SQLAllocHandle(SQL_HANDLE_DBC, ctx->handle(), &conn);
      odbc_report("odbc::basic_connection::create : SQLAllocHandle", res);
      res = SQLSetConnectAttr(conn, SQL_LOGIN_TIMEOUT, (SQLPOINTER*)5, 0);
      odbc_report("odbc::basic_connection::create : SQLSetConnectAttr", res);
      res = SQLConnect(conn,
                       (SQLCHAR*) host, SQL_NTS,
                       (SQLCHAR*) user, SQL_NTS,
                       (SQLCHAR*) pwd, SQL_NTS);
      odbc_report("odbc::basic_connection::create : SQLConnect", res);
      if(odbc_success(res)) {
        return std::unique_ptr<this_type>(new this_type(conn));
      }
    }
    return std::unique_ptr<this_type>(nullptr);
  }

public:
  friend void swap(basic_connection& l, basic_connection& r)
  {
    using std::swap;
    swap(l.connection_, r.connection_);
  }

private:
  native_handle connection_;
};

} // namespace odbc

}

#endif
