#ifndef ODBC_BASIC_CONTEXT_HPP_
#define ODBC_BASIC_CONTEXT_HPP_

#include "helpers.hpp"
#include "../core/non_copyable.hpp"

#include <memory>
#include <cstdlib>
#include <cstdio>

#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>

namespace fp
{

namespace odbc
{

class basic_context : non_copyable
{
public:
  typedef basic_context this_type;
  typedef SQLHENV native_handle;

  constexpr static native_handle kInvalidHandle = SQL_NULL_HANDLE;

protected:
  basic_context(SQLHENV env)
  : env_(env)
  { }

public:
  ~basic_context()
  {
    SQLFreeHandle(SQL_HANDLE_ENV, env_);
  }

  native_handle handle() const
  {
    return env_;
  }

  explicit operator bool() const
  {
    return (kInvalidHandle != env_);
  }

public:
  static std::unique_ptr<basic_context> create()
  {
    SQLHENV env = kInvalidHandle;
    SQLRETURN res;
    res = SQLAllocHandle(SQL_HANDLE_ENV, kInvalidHandle, &env);
    odbc_report("odbc::basic_context::create : SQLAllocHandle", res);
    res = SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, 0);
    odbc_report("odbc::basic_context::create : SQLSetEnvAttr", res);
    if(odbc_success(res)) {
      return std::unique_ptr<basic_context>(new basic_context(env));
    }
    return std::unique_ptr<basic_context>(nullptr);
  }

public:
  friend void swap(basic_context& l, basic_context& r)
  {
    using std::swap;
    swap(l.env_, r.env_);
  }

private:
  native_handle env_;
};

} // namespace odbc

}

#endif
