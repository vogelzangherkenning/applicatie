/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef _ODBC_RESULT_ITERATOR_HPP
#define _ODBC_RESULT_ITERATOR_HPP

#include "buffer_helper.hpp"
#include "helpers.hpp"
#include "../mpl/integer_sequence.hpp"

#include <iterator>
#include <stdexcept>
#include <utility>      // for std::swap

#include <sql.h>
#include <sqlext.h>
#include <sqltypes.h>

namespace fp
{

namespace odbc
{

namespace detail
{

template<std::size_t N, std::size_t... T>
struct gen_indices : gen_indices<(N - 1), (N - 1), T...> { };
template<std::size_t... Idx>
struct gen_indices<0, Idx...> : mpl::indices_<Idx...> { };

template<typename TRecord, std::size_t... Idx>
void bind_buffers(SQLHSTMT stmt, TRecord& record, mpl::indices_<Idx...>)
{
  using std::get;
  int ctx[] = { 0, (bind_buffer<(Idx + 1)>(stmt, get<Idx>(record)), void(), 0)... };
  (void)ctx;
}

template<template<typename...> class TRecord, typename... T>
void bind_buffers(SQLHSTMT stmt, TRecord<T...>& record)
{
  using indices = gen_indices<sizeof...(T)>;
  bind_buffers(stmt, record, indices{});
}

template<std::size_t Idx, typename T>
void fill_field(SQLHSTMT stmt, T& x)
{
  SQLLEN len;
  SQLGetData(stmt,
             Idx,
             SQL_C_DEFAULT,
             &x,
             sizeof(x),
             &len);
}

template<std::size_t Idx>
void fill_field(SQLHSTMT stmt, std::string& x)
{
  SQLLEN len;
  char buff[2048];
  SQLGetData(stmt,
             Idx,
             SQL_C_DEFAULT,
             &buff,
             sizeof(buff),
             &len);
  x = std::string(buff, buff + len);
}

template<std::size_t Idx, typename T>
void fill_field(SQLHSTMT stmt, T*& x)
{
  SQLLEN len;
  T* tmp = new T{};
  SQLGetData(stmt,
             Idx,
             SQL_C_DEFAULT,
             tmp,
             sizeof(*tmp),
             &len);
  if(len == SQL_NULL_DATA) {
    x = nullptr;
  } else {
    x = tmp;
  }
}

template<typename TRecord, std::size_t... Idx>
void fill_record(SQLHSTMT stmt, TRecord& record, mpl::indices_<Idx...>)
{
  int ctx[] = { 0, (fill_field<(Idx + 1)>(stmt, get<Idx>(record)), void(), 0)... };
  (void)ctx;
}

template<template<typename...> class TRecord, typename... T>
void fill_record(SQLHSTMT stmt, TRecord<T...>& record)
{
  using indices = gen_indices<sizeof...(T)>;
  fill_record(stmt, record, indices{});
}

} // namespace detail

template<typename T>
struct result_iterator
: std::iterator<std::input_iterator_tag, typename std::remove_pointer<T>::type>
{
public:
  using this_type = result_iterator;
  using base_type = std::iterator<std::input_iterator_tag, typename std::remove_pointer<T>::type>;
  using traits_type = std::iterator_traits<base_type>;

  using value_type = typename traits_type::value_type;
  using reference = typename traits_type::reference;
  using pointer = typename traits_type::pointer;
  using difference_type = typename traits_type::difference_type;
  using iterator_category = typename traits_type::iterator_category;

public:
  result_iterator()
  : stmt_(SQL_NULL_HSTMT)
  { }

  result_iterator(SQLHSTMT stmt)
  : stmt_(stmt)
  {
    SQLRETURN res = SQLFetch(stmt_);
    if(res == SQL_NO_DATA) {
      stmt_ = SQL_NULL_HSTMT;
    }
  }

  friend void swap(result_iterator& l, result_iterator& r)
  {
    using std::swap;
    swap(l.stmt_, r.stmt_);
  }

  result_iterator& operator++()
  {
    SQLRETURN res = SQLFetch(stmt_);
    if(res == SQL_NO_DATA) {
      stmt_ = SQL_NULL_HSTMT;
    }
    return *this;
  }

  result_iterator operator++(int)
  {
    SQLRETURN res = SQLFetch(stmt_);
    if(res == SQL_NO_DATA) {
      stmt_ = SQL_NULL_HSTMT;
    }
    return *this;
  }

  value_type operator*()
  {
    value_type ret;
    detail::fill_record(stmt_, ret);
    return ret;
  }

  friend bool operator!=(const result_iterator& l, const result_iterator& r)
  {
    return (l.stmt_ != r.stmt_);
  }

private:
  SQLHSTMT stmt_;
};

} // namespace odbc

}

#endif
