/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef _QUERY_COMBINER_HPP
#define _QUERY_COMBINER_HPP

#include "impl/query_combiner_impl.hpp"

namespace fp
{

template<typename...>
struct select_query;

namespace detail
{

template<typename... Columns>
using comb_normal = typename impl::query_combiner_impl<Columns...>::normal;

template<typename... Columns>
using comb_unique = typename impl::query_combiner_impl<Columns...>::unique;

}

template<
  typename... LColumns,
  typename... RColumns,
  typename = mpl::enable_if_t<mpl::all_<is_column<LColumns>...,
                                        is_column<RColumns>...>>
>
constexpr inline Invoke<typename detail::comb_normal<LColumns..., RColumns...>>
combine(const select_query<LColumns...>&,
        const select_query<RColumns...>&)
{
  return { };
}

template<
  typename... LColumns,
  typename... RColumns,
  typename = mpl::enable_if_t<mpl::all_<is_column<LColumns>...,
                                        is_column<RColumns>...>>
>
constexpr inline Invoke<typename detail::comb_unique<LColumns..., RColumns...>>
combine_unique(const select_query<LColumns...>&,
               const select_query<RColumns...>&)
{
  return { };
}

}

#endif
