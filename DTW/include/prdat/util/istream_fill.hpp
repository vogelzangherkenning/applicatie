#ifndef PRDAT_UTIL_ISTREAMFILL_HPP_
#define PRDAT_UTIL_ISTREAMFILL_HPP_

#include <algorithm>    // for std::find_if
#include <iostream>     // for std::istream
#include <iterator>     // for std::istream_iterator, std::back_insert_iterator

namespace util
{

template<typename Sequence>
static void istream_fill(std::istream& in, Sequence& seq)
{
  typedef typename Sequence::value_type value_type;
  typedef std::istream_iterator<value_type> input_iter;
  typedef std::back_insert_iterator<Sequence> output_iter;
  std::copy(input_iter(in), input_iter(), output_iter(seq));
}

template<typename Sequence>
static void r_istream_fill(std::istream& in, Sequence& seq, bool trim = true)
{
  Sequence local;
  double timing = 0.0;
  double frequency = 0.0;
  while(in) {
    if(!(in >> timing)) break;
    if(!(in >> frequency)) break;
    local.push_back(frequency);
  }

  if(trim) {
    typedef typename Sequence::iterator iterator;
    typedef typename Sequence::reverse_iterator reverse_iterator;

    struct non_zero
    {
      bool operator()(typename Sequence::value_type x) const
      { return x != 0; }
    };

    // Find first non-zero value, starting from the front
    iterator beg =  std::find_if(local.begin(),
                                 local.end(),
                                 non_zero());
    // Find first non-zero value, starting from the back
    reverse_iterator end =  std::find_if(local.rbegin(),
                                         local.rend(),
                                         non_zero());
    seq = Sequence(beg, end.base());
  } else {
    std::swap(local, seq);
  }
}

}


#endif
