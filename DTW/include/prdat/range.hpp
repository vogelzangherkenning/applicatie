#ifndef PRDAT_RANGE_HPP_
#define PRDAT_RANGE_HPP_

#include <iterator>     // for std::iterator_traits
#include <type_traits>  // for std::false_type, std::true_type

template<typename Iter>
struct iterator_range
{
public:
  typedef iterator_range this_type;
  typedef Iter iterator;
  typedef typename std::iterator_traits<iterator>::value_type value_type;
  typedef typename std::iterator_traits<iterator>::reference reference;
  typedef typename std::iterator_traits<iterator>::difference_type difference_type;

public:
  iterator_range(Iter begin, Iter end)
  : begin_(begin)
  , end_(end)
  { }

  iterator begin() const
  { return begin_; }

  iterator end() const
  { return end_; }

  difference_type size() const
  { return std::distance(begin_, end_); }

  reference operator[](int i) const
  { return *std::next(begin_, i); }

public:
  friend iterator begin(const iterator_range& self) { return self.begin(); }
  friend iterator end(const iterator_range& self) { return self.end(); }

private:
  iterator begin_;
  iterator end_;
};

template<typename>
struct is_range : std::false_type { };
template<typename Iter>
struct is_range<iterator_range<Iter>> : std::true_type { };

template<typename Iter>
iterator_range<Iter> make_range(const iterator_range<Iter>& range)
{
  return range;
}

template<typename Iter>
iterator_range<Iter> make_range(Iter first, Iter last)
{
  return iterator_range<Iter>(first, last);
}

template<typename Sequence>
iterator_range<typename Sequence::iterator> make_range(Sequence& seq)
{
  return iterator_range<typename Sequence::iterator>(seq.begin(), seq.end());
}

template<typename Sequence>
iterator_range<typename Sequence::const_iterator> make_range(const Sequence& seq)
{
  return iterator_range<typename Sequence::const_iterator>(seq.begin(), seq.end());
}

#endif
