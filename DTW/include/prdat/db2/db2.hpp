#ifndef PRDAT_DB2_HPP_
#define PRDAT_DB2_HPP_

#include "../../../database.hpp"
#include "../../fp/select_query.hpp"
#include "../../fp/where_query.hpp"
#include "../../fp/where_select_query.hpp"
#include "../../fp/mysql/basic_engine.hpp"

#include <string>   // for std::string
#include <utility>  // for std::move
#include <vector>   // for std::vector

typedef std::vector<std::string> pathlist_t;
typedef std::vector<int> birdlist_t;

namespace db2
{

template<typename Engine>
pathlist_t get_file_paths(Engine& engine)
{
  pathlist_t paths;
  auto qry = fp::select(birdshaz::db::birdsamples::path);
  auto res = fp::query(engine, qry);

  for(const auto& cur : res)
  {
    auto path = get(cur, birdshaz::db::birdsamples::path);
    paths.push_back(std::move(path));
  }

  return paths;
}

template<typename Engine>
birdlist_t get_birds_by_path(Engine& engine, const std::string& path)
{
  birdlist_t birds;
  auto sel = fp::select(birdshaz::db::birdsamples::bird);
  auto where = fp::where(fp::eq(birdshaz::db::birdsamples::path, path));
  auto qry = sel + where;
  auto res = fp::query(engine, qry);

  for(const auto& cur : res)
  {
    auto id = get(cur, birdshaz::db::birdsamples::bird);
    birds.push_back(id);
  }

  return birds;
}

}

#endif
