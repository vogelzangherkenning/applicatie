#ifndef PRDAT_ALGO_KIM_HPP_
#define PRDAT_ALGO_KIM_HPP_

#include <algorithm>  // for std::max_element, std::min_element
#include <cmath>      // for std::abs

namespace algo
{

/**
 * Kim lower bound algorithm
*/
class kim
{
public:
  typedef kim this_type;

public:
  template<typename Sequence>
  double operator()(const Sequence& Q, const Sequence& C) const
  {
    using std::begin; using std::end;
    const double diffs[] = {
      std::abs(*Q.begin() - *C.begin()),
      std::abs(*(Q.end() - 1) - *(C.end() - 1)),
      std::abs(*std::min_element(Q.begin(), Q.end()) -
               *std::min_element(C.begin(), C.end())),
      std::abs(*std::max_element(Q.begin(), Q.end()) -
               *std::max_element(C.begin(), C.end())),
    };
    return *std::max_element(begin(diffs), end(diffs));
  }

};

}

#endif
