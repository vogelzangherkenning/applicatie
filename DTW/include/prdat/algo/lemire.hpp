#ifndef PRDAT_ALGO_LEMIRE_HPP_
#define PRDAT_ALGO_LEMIRE_HPP_

#include "keogh.hpp"

namespace algo
{

/**
 * Lemire lower bound algorithm
*/
class lemire
{
public:
  typedef lemire this_type;

public:
  lemire(std::size_t r)
  : r_(r)
  { }

  template<typename Sequence>
  double operator()(const Sequence& Q, const Sequence& C) const
  {
    keogh k(r_);
    return k(Q, C) + k(Q, H(Q, C, r_));
  }

private:
  template<typename Sequence>
  static Sequence H(const Sequence& Q, const Sequence& C, std::size_t r)
  {
    typedef typename Sequence::value_type value_type;
    typedef typename Sequence::size_type size_type;
    Sequence res;
    for(size_type i = 0; i < Q.size(); ++i) {
      const value_type LQ = keogh::lower(Q, i, r);
      const value_type UQ = keogh::upper(Q, i, r);
      if(C[i] >= UQ) {
        res.push_back(UQ);
      } else if(C[i] <= LQ) {
        res.push_back(LQ);
      } else {
        res.push_back(C[i]);
      }
    }
    return res;
  }

  const std::size_t r_;
};

}

#endif
