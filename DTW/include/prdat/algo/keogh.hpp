#ifndef PRDAT_ALGO_KEOGH_HPP_
#define PRDAT_ALGO_KEOGH_HPP_

#include "../util.hpp"  // for util::clamp

#include <algorithm>    // for std::max_element, std::min_element
#include <cmath>        // for std::pow, std::sqrt

namespace algo
{

/**
 * Keogh lower bound algorithm
*/
class keogh
{
public:
  typedef keogh this_type;

public:
  keogh(std::size_t r)
  : r_(r)
  { }

  template<typename Sequence>
  double operator()(const Sequence& Q, const Sequence& C) const
  {
    typedef typename Sequence::value_type value_type;
    typedef typename Sequence::size_type size_type;

    double total = 0;
    for(size_type i = 0; i < C.size(); ++i) {
      const value_type LQ = this_type::lower(Q, i, r_);
      const value_type UQ = this_type::upper(Q, i, r_);
      if(C[i] > UQ) {
        total += std::pow((C[i] - UQ), 2);
      } else if(C[i] < LQ) {
        total += std::pow((C[i] - LQ), 2);
      } else {
        // total += 0
      }
    }
    return std::sqrt(total);
  }

public:
  template<
    typename Sequence,
    typename ValueType = typename Sequence::value_type
  >
  static ValueType lower(const Sequence& Q, int i, int r)
  {
    using std::begin;
    const int idx_min = util::clamp(i - r, 0, Q.size());
    const int idx_max = util::clamp(i + r, 0, Q.size());
    return *std::min_element(std::next(begin(Q), idx_min),
                             std::next(begin(Q), idx_max));
  }

  template<
    typename Sequence,
    typename ValueType = typename Sequence::value_type
  >
  static ValueType upper(const Sequence& Q, int i, int r)
  {
    using std::begin;
    const int idx_min = util::clamp(i - r, 0, Q.size());
    const int idx_max = util::clamp(i + r, 0, Q.size());
    return *std::max_element(std::next(begin(Q), idx_min),
                             std::next(begin(Q), idx_max));
  }

private:
  const std::size_t r_;
};

}

#endif
