#ifndef PRDAT_GENERATOR_ITERATOR_HPP_
#define PRDAT_GENERATOR_ITERATOR_HPP_

#include "iterator.hpp"

template<typename Generator>
class step_iterator
{
public:
  typedef step_iterator this_type;
  typedef typename Generator::step_type step_type;

  typedef forward_iterator<typename Generator::step_type> base_type;

  typedef typename base_type::iterator_category iterator_category;
  typedef typename base_type::value_type value_type;
  typedef typename base_type::reference reference;
  typedef typename base_type::pointer pointer;
  typedef typename base_type::difference_type difference_type;

public:
  step_iterator(Generator& gen, step_type init)
  : gen_(&gen)
  , step_(init)
  { }

  reference operator*()
  {
    return step_;
  }

  pointer operator->()
  {
    return &(operator*());
  }

  step_iterator operator++(int)
  {
    step_iterator copy = *this;
    step_ = gen_->next(step_);
    return copy;
  }

  step_iterator& operator++()
  {
    step_ = gen_->next(step_);
    return *this;
  }

  bool operator==(const step_iterator& it) const
  {
    return (step_ == it.step_);
  }

  bool operator!=(const step_iterator& it) const
  {
    return (step_ != it.step_);
  }

private:
  Generator* gen_;
  step_type step_;
};

template<typename Generator>
class reverse_step_iterator {
public:
  typedef reverse_step_iterator this_type;
  typedef typename Generator::step_type step_type;

  typedef std::forward_iterator_tag iterator_category;
  typedef step_type value_type;
  typedef step_type& reference;
  typedef step_type* pointer;
  typedef std::size_t difference_type;

public:
  reverse_step_iterator(Generator& gen, step_type init)
  : gen_(gen)
  , step_(init)
  { }

  reference operator*()
  {
    return step_;
  }

  pointer operator->()
  {
    return &(operator*());
  }

  reverse_step_iterator operator++(int)
  {
    reverse_step_iterator copy = *this;
    step_ = gen_.prev(step_);
    return copy;
  }

  reverse_step_iterator& operator++()
  {
    step_ = gen_.prev(step_);
    return *this;
  }

  bool operator==(const reverse_step_iterator& it) const
  {
    return (step_ == it.step_);
  }

  bool operator!=(const reverse_step_iterator& it) const
  {
    return (step_ != it.step_);
  }

private:
  Generator& gen_;
  step_type step_;
};

#endif
