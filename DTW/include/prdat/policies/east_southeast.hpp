#ifndef PRDAT_EAST_SOUTHEAST_HPP_
#define PRDAT_EAST_SOUTHEAST_HPP_

#include <algorithm>    // for std::sort
#include <cstdint>      // for std::size_t

template<typename BoundaryPolicy>
struct EastSouthEast
{
public:
  typedef EastSouthEast this_type;
  typedef std::pair<int, int> step_type;
  typedef BoundaryPolicy boundary_policy;

  static const step_type invalid_step;

public:
  template<
    typename Matrix,
    typename = typename Matrix::size_type
  >
  static step_type begin(const Matrix& mat)
  {
    return step_type(0, 0);
  }

  template<
    typename Matrix,
    typename = typename Matrix::size_type
  >
  static step_type end(const Matrix& mat)
  {
    return step_type(mat.rows(), mat.columns());
  }

  template<
    typename Matrix,
    typename = typename Matrix::size_type
  >
  static step_type rbegin(const Matrix& mat)
  {
    return step_type(mat.rows() - 1, mat.columns() - 1);
  }

  template<
    typename Matrix,
    typename = typename Matrix::size_type
  >
  static step_type rend(const Matrix& mat)
  {
    return step_type(-1, -1);
  }

  template<
    typename Matrix,
    typename = typename Matrix::size_type
  >
  static step_type next(const step_type& cur,
                        const Matrix& mat,
                        const boundary_policy& policy)
  {
    static const std::size_t num_steps = 3;
    // All possible steps, put the preferred ones first!
    static step_type steps[] = {
      step_type(  1, 1 ),  // SOUTH EAST
      step_type(  0, 1 ),  // EAST
      step_type(  1, 0),   // SOUTH
    };
    // Local data
    int indices[] = { 0, 1, 2, };
    double vals[] = { 0, 0, 0, };
    bool valid[] = { false, false, false, };
    // Check validity of step, and if valid get value
    for(std::size_t i = 0; i < num_steps; ++i) {
      const step_type next = step_type(cur.first + steps[i].first, cur.second + steps[i].second);
      valid[i] = policy.validate(cur, next);
      if(valid[i]) {
        vals[i] = mat[next.first][next.second];
      }
    }
    // Sort indices based on values
    std::sort(std::begin(indices),
              std::end(indices),
              [&vals](int l, int r) {
                return (vals[l] < vals[r]);
              });
    // If we have a valid step, adjust row/col and return
    for(std::size_t i = 0; i < num_steps; ++i) {
      const int idx = indices[i];
      if(valid[idx]) {
        return steps[idx];
      }
    }
    // No valid move found
    return invalid_step;
  }

  template<
    typename Matrix,
    typename = typename Matrix::size_type
  >
  static step_type prev(const step_type& cur,
                        const Matrix& mat,
                        const boundary_policy& policy)
  {
    static const std::size_t num_steps = 3;
    // All possible steps, put the preferred ones first!
    static step_type steps[] = {
      step_type( -1, -1 ),  // SOUTH EAST
      step_type(  0, -1 ),  // EAST
      step_type( -1,  0 ),  // SOUTH
    };
    // Local data
    int indices[] = { 0, 1, 2, };
    double vals[] = { 0, 0, 0, };
    bool valid[] = { false, false, false, };
    // Check validity of step, and if valid get value
    for(std::size_t i = 0; i < num_steps; ++i) {
      const step_type next = step_type(cur.first + steps[i].first, cur.second + steps[i].second);
      valid[i] = policy.validate(cur, next);
      if(valid[i]) {
        vals[i] = mat[next.first][next.second];
      }
    }
    // Sort indices based on values
    std::sort(std::begin(indices),
              std::end(indices),
              [&vals](int l, int r) {
                return (vals[l] < vals[r]);
              });
    // If we have a valid step, adjust row/col and return
    for(std::size_t i = 0; i < num_steps; ++i) {
      const int idx = indices[i];
      if(valid[idx]) {
        return steps[idx];
      }
    }
    // No valid move found
    return invalid_step;
  }

  static bool validate(const step_type& step)
  {
    return (invalid_step != step);
  }

};

template<typename BoundaryPolicy>
const typename EastSouthEast<BoundaryPolicy>::step_type
EastSouthEast<BoundaryPolicy>::invalid_step = EastSouthEast<BoundaryPolicy>::step_type(0, 0);

#endif
