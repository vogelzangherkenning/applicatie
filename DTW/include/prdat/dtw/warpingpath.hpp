#ifndef PRDAT_DTW_WARPINGPATH_HPP_
#define PRDAT_DTW_WARPINGPATH_HPP_

#include "../core/matrix.hpp"

#include <algorithm>  // for std::accumulate, std::find
#include <tuple>      // for std::pair
#include <deque>      // for std::vector

namespace dtw
{

struct forward_path_t { };
struct reverse_path_t { };
struct combine_path_t { };

static const forward_path_t forward_path = forward_path_t();
static const reverse_path_t reverse_path = reverse_path_t();
static const combine_path_t combine_path = combine_path_t();

typedef std::deque<std::pair<int, int> > warpingpath;

/*
 * Remove unnecessary steps from our path.
 * Unneccessary means that
 * x o         x x            x o
 * x x   and   o x   become   o x
 *
*/
template<typename Path>
Path cheapify_path(const Path& path)
{
  typedef typename Path::value_type value_type;
  typedef typename Path::size_type size_type;

  Path cheap;
  const size_type sz = path.size();
  size_type i = 0;
  while(i < (sz - 2)) {
    const value_type& cur = path[i];
    const value_type& nnext = path[i + 2];
    cheap.push_back(cur);
    if(((nnext.first - cur.first) == 1) &&
       ((nnext.second - cur.second) == 1)) {
      i += 2;
    } else {
      i += 1;
    }
  }
  for(; i < sz; ++i) {
    cheap.push_back(path[i]);
  }
  return cheap;
}

template<
  typename Matrix,
  typename Path,
  typename = typename Path::value_type
>
double get_path_cost(const Path& path, const Matrix& matrix)
{
  typedef typename Path::value_type value_type;

  struct accumulator {
  public:
    accumulator(const Matrix& matrix)
    : matrix_(matrix)
    { }

    double operator()(double init, const value_type& step) const
    {
      return init + matrix_[step.first][step.second];
    }
  private:
    const Matrix& matrix_;
  };

  using std::begin;
  using std::end;

  return std::accumulate(begin(path),
                         end(path),
                         0.0,
                         accumulator(matrix));
}

template<
  typename Matrix,
  typename Generator,
  typename = typename Matrix::size_type,
  typename = typename Generator::iterator
>
warpingpath get_warpingpath(const Matrix& matrix, Generator gen, forward_path_t)
{
  warpingpath path;
  std::copy(gen.begin(), gen.end(), std::back_inserter(path));
  std::sort(path.begin(), path.end());
  return cheapify_path(path);
}

template<
  typename Matrix,
  typename Generator,
  typename = typename Matrix::size_type,
  typename = typename Generator::iterator
>
warpingpath get_warpingpath(const Matrix& matrix, Generator gen, reverse_path_t)
{
  warpingpath path;
  std::copy(gen.rbegin(), gen.rend(), std::back_inserter(path));
  std::sort(path.begin(), path.end());
  return cheapify_path(path);
}

template<
  typename Matrix,
  typename Generator,
  typename = typename Matrix::size_type,
  typename = typename Generator::iterator
>
warpingpath get_warpingpath(const Matrix& matrix, Generator gen, combine_path_t)
{
  typedef typename warpingpath::reverse_iterator reverse_iterator;

  warpingpath forward = get_warpingpath(matrix, gen, forward_path);
  warpingpath reverse = get_warpingpath(matrix, gen, reverse_path);
  // Combine both paths to get true cheapest path
  for(reverse_iterator reverse_it = reverse.rbegin() + 1;
      reverse_it != reverse.rend();
      ++reverse_it) {
    reverse_iterator forward_it = std::find(forward.rbegin(),
                                                         forward.rend(),
                                                         *reverse_it);
    if(forward_it != forward.rend()) {
      warpingpath combine;
      std::copy(forward.begin(),
                forward_it.base(),
                std::back_inserter(combine));
      std::copy(reverse_it.base(),
                reverse.end(),
                std::back_inserter(combine));
      return combine;
    }
  }
  // This should not happen (at all), but either way return the cheapest path
  if(get_path_cost(forward, matrix) <= get_path_cost(reverse, matrix)) {
    return forward;
  } else {
    return reverse;
  }
}

template<
  typename Matrix,
  typename Generator,
  typename = typename Matrix::size_type,
  typename = typename Generator::iterator
>
inline warpingpath get_warpingpath(const Matrix& matrix, Generator gen)
{
  return get_warpingpath(matrix, gen, combine_path);
}

}

#endif
