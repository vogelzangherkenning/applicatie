#ifndef PRDAT_DTW_NORMALIZE_HPP_
#define PRDAT_DTW_NORMALIZE_HPP_

#include "../range.hpp"

#include <algorithm>    // for std::accumulate, std::inner_product, std::transform
#include <cmath>        // for std::sqrt
#include <functional>   // for std::bind

namespace dtw
{

namespace detail
{

/*
* Most accurate method as found on
* http://stackoverflow.com/a/7616783/840382
*/
template<typename Sequence>
inline void get_sequence_info(const Sequence& seq, double* mean, double* dev)
{
  using std::begin; using std::end;
  const double sum = std::accumulate(begin(seq), end(seq), 0.0);
  *mean = (sum / seq.size());
  std::vector<double> diff(seq.size());
  std::transform(begin(seq),
                 end(seq),
                 begin(diff),
                 [&](double x) {
                  return (x - *mean);
                 });
  *dev = std::sqrt(std::inner_product(begin(diff),
                                      end(diff),
                                      begin(diff),
                                      0.0
                                      ) / seq.size());
}

}

template<typename Sequence>
static Sequence normalize(const Sequence& r)
{
  struct normalizer
  {
  public:
    normalizer(double mean, double dev)
    : mean_(mean)
    , dev_(dev)
    { }

    inline double operator()(double x) const
    {
      return (x - mean_) / dev_;
    }

  private:
    double mean_;
    double dev_;
  };

  using std::begin; using std::end;

  Sequence res(r.size());
  double mean = 0, dev = 0;
  detail::get_sequence_info(r, &mean, &dev);
  std::transform(begin(r),
                 end(r),
                 begin(res),
                 normalizer(mean, dev)
                );
  return res;
}

}

#endif
