#ifndef PRDAT_DTW_UNIFORM_SCALING_HPP_
#define PRDAT_DTW_UNIFORM_SCALING_HPP_

#include <algorithm>    // std::max_element, std::min_element
#include <cmath>        // for std::pow, std::sqrt

namespace dtw
{

namespace detail
{

template<typename Sequence>
inline double normalize_lb_one(const Sequence& r, std::size_t i, double factor)
{
  return *std::min_element(r.begin + (i / factor), r.begin() + (i * factor));
}

template<typename Sequence>
inline double normalize_ub_one(const Sequence& r, std::size_t i, double factor)
{
  return *std::max_element(r.begin + (i / factor), r.begin() + (i * factor));
}

inline std::size_t get_optimal_size(std::size_t orig, std::size_t max)
{
  return orig + ((max - orig) / 2);
}

template<typename QSequence, typename CSequence>
static double normalize_lb(const QSequence& Q,
                           const CSequence& C,
                           double factor
                          )
{
  double total = 0;
  for(std::size_t i = 0; i < Q.size(); ++i) {
    const double U = normalize_lb_one(C, i, factor);
    const double L = normalize_ub_one(C, i, factor);
    if(Q[i] > U) {
      total += std::pow((Q[i] - U), 2);
    } else if(Q[i] < L) {
      total += std::pow((Q[i] - L), 2);
    } else {
      // total += 0
    }
  }
  return total;
}

}

template<typename QSequence, typename CSequence>
static QSequence uniform_scaling(const QSequence& Q, const CSequence& C)
{

}

}

#endif
