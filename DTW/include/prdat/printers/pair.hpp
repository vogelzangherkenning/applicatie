#ifndef PRDAT_PRINTERS_PAIR_HPP_
#define PRDAT_PRINTERS_PAIR_HPP_

#include <iostream>   // for std::ostream
#include <tuple>      // for std::pair

namespace printers
{

template<typename Pair>
class pair_printer
{
public:
  typedef pair_printer this_type;
  typedef Pair pair_type;

public:
  pair_printer(const pair_type& pair)
  : pair_(pair)
  { }

public:
  friend std::ostream& operator<<(std::ostream& out, const pair_printer& self)
  {
    out << '[' << self.pair_.first << ", " << self.pair_.second << ']';
    return out;
  }

private:
  const pair_type& pair_;
};

}

template<typename T1, typename T2>
printers::pair_printer<std::pair<T1, T2>> pretty_print(const std::pair<T1, T2>& p)
{
  return printers::pair_printer<std::pair<T1, T2>>(p);
}

#endif
