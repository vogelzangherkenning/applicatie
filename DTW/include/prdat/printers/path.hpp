#ifndef PRDAT_PRINTERS_PATH_HPP_
#define PRDAT_PRINTERS_PATH_HPP_

#include <iostream>

namespace printers
{

template<typename Path, typename Matrix>
class path_printer
{
public:
  typedef path_printer this_type;
  typedef Matrix matrix_type;
  typedef Path path_type;

public:
  path_printer(const path_type& path, const matrix_type& matrix)
  : matrix_(matrix)
  , path_(path)
  { }

public:
  friend std::ostream& operator<<(std::ostream& out, const path_printer& self)
  {
    typedef typename path_type::const_iterator iterator;
    typedef typename matrix_type::size_type size_type;

    matrix_type mat(self.matrix_.rows(), self.matrix_.columns());
    for(iterator it = self.path_.begin(); it != self.path_.end(); ++it) {
      const size_type r = it->first;
      const size_type c = it->second;
      mat[r][c] = self.matrix_[r][c];
    }

    out << pretty_print(mat);
    return out;
  }

private:
  const matrix_type& matrix_;
  const path_type& path_;
};

}

template<typename Path, typename Matrix>
printers::path_printer<Path, Matrix> pretty_print(const Path& path,
                                                  const Matrix& mat)
{
  return printers::path_printer<Path, Matrix>(path, mat);
}

#endif
