#ifndef PRDAT_PAA_HPP_
#define PRDAT_PAA_HPP_

#include "../iterators/stateful_iterator.hpp"

#include <algorithm>
#include <cmath>
#include <iterator>
#include <stdexcept>

namespace dtw
{

namespace detail
{

template<typename Owner>
struct paa_state
{
public:
  typedef paa_state this_type;
  typedef typename Owner::size_type size_type;

  typedef forward_iterator<double,
                           std::ptrdiff_t,
                           double,
                           double> base_type;

  typedef typename base_type::iterator_category iterator_category;
  typedef typename base_type::value_type value_type;
  typedef typename base_type::reference reference;
  typedef typename base_type::pointer pointer;
  typedef typename base_type::difference_type difference_type;

public:
  paa_state(Owner& owner, size_type index)
  : owner_(&owner)
  , index_(index)
  { }

  paa_state operator++(int)
  {
    paa_state copy = *this;
    ++index_;
    return copy;
  }

  paa_state& operator++()
  {
    ++index_;
    return *this;
  }

  paa_state operator--(int)
  {
    paa_state copy = *this;
    --index_;
    return copy;
  }

  paa_state& operator--()
  {
    --index_;
    return *this;
  }

  double operator*()
  {
    return owner_->reduce(index_);
  }

  bool operator==(const paa_state& other) const
  {
    return (index_ == other.index_);
  }

private:
  Owner* owner_;
  size_type index_;
};

}

template<typename Sequence>
class PAA
{
public:
  typedef PAA this_type;
  typedef Sequence sequence_type;
  typedef std::size_t size_type;
  typedef detail::paa_state<PAA> state_type;

  typedef double value_type;

  typedef stateful_iterator<state_type> iterator;
  typedef std::reverse_iterator<iterator> reverse_iterator;

public:
  PAA(const sequence_type& sequence, size_type width)
  : sequence_(sequence)
  , width_(width)
  { }

  iterator begin()
  {
    return iterator(state_type(*this, 0));
  }

  iterator end()
  {
    const size_type index = ((sequence_.size() + (width_ - 1)) / width_);
    return iterator(state_type(*this, index));
  }

  reverse_iterator rbegin()
  {
    return reverse_iterator(end());
  }

  reverse_iterator rend()
  {
    return reverse_iterator(begin());
  }

  value_type reduce(size_type index)
  {
    typedef typename sequence_type::const_iterator const_iterator;

    const size_type idx_front = std::min(sequence_.size(), index * width_);
    const size_type idx_back = std::min(sequence_.size(), idx_front + width_);
    const_iterator first = std::next(sequence_.begin(), idx_front);
    const_iterator last = std::next(sequence_.begin(), idx_back);
    return std::accumulate(first, last, 0.0) / width_;
  }

private:
  const sequence_type& sequence_;
  size_type width_;
};

}

template<typename Sequence>
dtw::PAA<Sequence> make_paa(const Sequence& seq, std::size_t width)
{
  return dtw::PAA<Sequence>(seq, width);
}

#endif
