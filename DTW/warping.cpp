#include "include/prdat/algo.hpp"
#include "include/prdat/dtw.hpp"
#include "include/prdat/policies.hpp"
#include "include/prdat/printers.hpp"
#include "include/prdat/reducers.hpp"
#include "include/prdat/util.hpp"

#include "include/node/helpers.hpp"

#include <node/node.h>
#include <v8.h>
#include <vector>

using namespace v8;

// Wrapper arround dtw::normalize
Handle<Value> Normalize(const Arguments& args)
{
  HandleScope scope;

  if(args.Length() != 1) {
    ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
    return scope.Close(Undefined());
  }

  if(!args[0]->IsArray()) {
    ThrowException(Exception::TypeError(String::New("Argument 1 should be an array")));
    return scope.Close(Undefined());
  }

  Local<Array> data = Array::Cast(*args[0]);

  std::vector<double> vector = dtw::normalize(arrayToDoubleVector(data));
  return scope.Close(doubleVectorToArray(vector));
}

// TODO: get_path_cost
Handle<Value> GetWarpingMatrix(const Arguments& args)
{
  HandleScope scope;

  if(args.Length() != 2) {
    ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
    return scope.Close(Undefined());
  }

  // args[0] is the spectogram
  if(!args[0]->IsArray()) {
    ThrowException(Exception::TypeError(String::New("Argument 1 should be an array")));
    return scope.Close(Undefined());
  }

  // args[1] is the sample
  if(!args[1]->IsNumber()) {
    ThrowException(Exception::TypeError(String::New("Argument 2 should be an array")));
    return scope.Close(Undefined());
  }

  Local<Array> aQ = Array::Cast(*args[0]);
  Local<Array> aC = Array::Cast(*args[1]);

  dtw::warpingmatrix<double> warp = dtw::get_warpingmatrix(arrayToDoubleVector(aQ),
                                                           arrayToDoubleVector(aC));
  return scope.Close(matrixToArray(warp));
}

// PAA
Handle<Value> ReducePaa(const Arguments& args)
{
  HandleScope scope;

  if(args.Length() != 2) {
    ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
    return scope.Close(Undefined());
  }

  // args[0] is the spectogram
  if(!args[0]->IsArray()) {
    ThrowException(Exception::TypeError(String::New("Argument 1 should be an array")));
    return scope.Close(Undefined());
  }

  // args[1] is the number of dimensions
  if(!args[1]->IsNumber()) {
    ThrowException(Exception::TypeError(String::New("Argument 2 should be an integer")));
    return scope.Close(Undefined());
  }

  Local<Array> data = Array::Cast(*args[0]);
  double dimensions = args[1]->NumberValue();

  std::vector<double> vector = arrayToDoubleVector(data);
  vector = dtw::reduce(vector, make_paa((int)dimensions, vector));

  return scope.Close(doubleVectorToArray(vector));
}

void init(Handle<Object> exports) {
  // Export Normalize
  exports->Set(String::NewSymbol("normalize"),
               FunctionTemplate::New(Normalize)->GetFunction());

  exports->Set(String::NewSymbol("reduce"),
               FunctionTemplate::New(ReducePaa)->GetFunction());
}

NODE_MODULE(warping, init)
