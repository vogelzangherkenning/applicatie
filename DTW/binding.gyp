{
	"targets": [
		{
			"target_name": "warping",
			"sources": [ "warping.cpp" ],
			"libraries": [ "../bin/dtw.o" ],
			"cflags": ["-std=c++11","-stdlib=libc++"],
			"xcode_settings": {
				"OTHER_CFLAGS": [
					"-std=c++11",
					"-stdlib=libc++"
				],
				"MACOSX_DEPLOYMENT_TARGET": "10.8"
			}
		}
	]
}