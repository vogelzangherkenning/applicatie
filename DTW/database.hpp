#ifndef PRDAT_DATABASE_HPP_
#define PRDAT_DATABASE_HPP_

#include "include/fp/column.hpp"
#include "include/fp/field.hpp"
#include "include/fp/primary_key.hpp"
#include "include/fp/table.hpp"
#include "include/fp/mpl/identity.hpp"
#include "include/fp/mpl/type_sequence.hpp"

namespace birdshaz
{

namespace db
{

namespace strings
{

constexpr static char const birds[] = "BIRDSHAZ.BIRDS";
constexpr static char const birdsamples[] = "BIRDSHAZ.BIRDSAMPLES";

constexpr static char const birdid[] = "BIRDID";
constexpr static char const birdname[] = "BIRDNAME";
constexpr static char const family[] = "FAMILY";
constexpr static char const genus[] = "GENUS";
constexpr static char const species[] = "SPECIES";

constexpr static char const sampleid[] = "SAMPLEID";
constexpr static char const sampletype[] = "SAMPLETYPE";
constexpr static char const samplepath[] = "SAMPLEPATH";

} // namespace strings

struct birds
{
  constexpr static fp::table<birds, strings::birds> table = { };

  constexpr static fp::column<birds, strings::birdid, fp::field<int>> id = { };
  constexpr static fp::column<birds, strings::birdname, fp::field<std::string>> name = { };
  constexpr static fp::column<birds, strings::family, fp::field<std::string>> family = { };
  constexpr static fp::column<birds, strings::genus, fp::field<std::string>> genus = { };
  constexpr static fp::column<birds, strings::species, fp::field<std::string>> species = { };
}; // struct birds

constexpr fp::table<birds, strings::birds> birds::table;

constexpr fp::column<birds, strings::birdid, fp::field<int>> birds::id;
constexpr fp::column<birds, strings::birdname, fp::field<std::string>> birds::name;
constexpr fp::column<birds, strings::family, fp::field<std::string>> birds::family;
constexpr fp::column<birds, strings::genus, fp::field<std::string>> birds::genus;
constexpr fp::column<birds, strings::species, fp::field<std::string>> birds::species;

struct birdsamples
{
  constexpr static fp::table<birdsamples, strings::birdsamples> table = { };

  constexpr static fp::column<birdsamples, strings::sampleid, fp::field<int>> id = { };
  constexpr static fp::column<birdsamples, strings::birdid, fp::field<int>> bird = { };
  constexpr static fp::column<birdsamples, strings::sampletype, fp::field<std::string>> type = { };
  constexpr static fp::column<birdsamples, strings::samplepath, fp::field<std::string>> path = { };
}; // struct birdsamples

constexpr fp::table<birdsamples, strings::birdsamples> birdsamples::table;

constexpr fp::column<birdsamples, strings::sampleid, fp::field<int>> birdsamples::id;
constexpr fp::column<birdsamples, strings::birdid, fp::field<int>> birdsamples::bird;
constexpr fp::column<birdsamples, strings::sampletype, fp::field<std::string>> birdsamples::type;
constexpr fp::column<birdsamples, strings::samplepath, fp::field<std::string>> birdsamples::path;

} // namespace db

} // namespace birdshaz

#endif // PRDAT_DATABASE_HPP_
