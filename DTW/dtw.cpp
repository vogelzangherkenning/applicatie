#include "include/prdat/algo.hpp"
#include "include/prdat/dtw.hpp"
#include "include/prdat/policies.hpp"
#include "include/prdat/printers.hpp"
#include "include/prdat/reducers.hpp"
#include "include/prdat/util.hpp"

#include <fstream>      // for std::ifstream
#include <iostream>     // for std::cout
#include <vector>       // for std::vector

/*
 * Normalization of input data will happen NORMALIZATION_ROUNDS times
*/
#if !defined(NORMALIZATION_ROUNDS)
  #define NORMALIZATION_ROUNDS 0
#endif

#if !defined(REDUCED_DIMENSION_SIZE)
  #define REDUCED_DIMENSION_SIZE 100
#endif

/*
 * Define PRDAT_INPUT_ARGS if you want to specify
 * input files as arguments to the application
*/
//#define PRDAT_INPUT_ARGS

const char* q_file = "lq.txt";
const char* c_file = "lc.txt";

int main(int argc, char** argv)
{
#if defined(PRDAT_INPUT_ARGS)
  if(argc < 3) {
    std::cout << "Usage: dtw <samplefile> <queryfile>"
              << std::endl;
    std::exit(0);
  }
#endif

  switch(argc)
  {
    case 3:
      q_file = argv[2];
    case 2:
      c_file = argv[1];
  }

  std::vector<double> q;
  std::vector<double> c;

  { // Open input files in this scope so they
    // get closed after we are done with them
    std::ifstream fq(q_file);
    std::ifstream fc(c_file);

    if(!fq.is_open()) {
      std::cerr << "aborting: Could not open Q input file"
                << std::endl;
      std::exit(1);
    }

    if(!fc.is_open()) {
      std::cerr << "aborting: Could not open C input file"
                << std::endl;
      std::exit(2);
    }

    util::r_istream_fill(fq, q);
    util::r_istream_fill(fc, c);

    std::cout << "Original data:"
              << std::endl
              << "Q: " << q.size() << " elements"
              << std::endl
              << "C: " << c.size() << " elements"
              << std::endl;
  }

  // Q's size must not be larger than C's size
  if(q.size() > c.size()) {
    std::cerr << "aborting: Q.size() > C.size()"
              << std::endl;
    std::exit(3);
  }

  // Normalize our data
  for(int i = 0; i < NORMALIZATION_ROUNDS; ++i) {
    q = dtw::normalize(q);
    c = dtw::normalize(c);
  }

  // Reduce dimensionality using PAA
  q = dtw::reduce(q, make_paa(q, REDUCED_DIMENSION_SIZE));
  c = dtw::reduce(c, make_paa(c, REDUCED_DIMENSION_SIZE));

  std::cout << "Reduced data:"
            << std::endl;
  std::cout << "Q: "
            << pretty_print(q)
            << std::endl;
  std::cout << "C: "
            << pretty_print(c)
            << std::endl;

  std::cout << std::endl;

  std::cout << "Lemire distance: "
            << dtw::lowerbound(q, c, algo::lemire(3))
            << std::endl;

  std::cout << std::endl;

  // Calculate our warping matrix
  dtw::warpingmatrix<double> warp = dtw::get_warpingmatrix(q, c);
  std::cout << "Warping matrix:"
            << std::endl;
  std::cout << pretty_print(warp)
            << std::endl;

  std::cout << std::endl;

  // Get warping path using Sakoe-Chiba band with a width of 3
  dtw::warpingpath path = dtw::get_warpingpath(warp, make_sakoechiba<EastSouthEast>(3, warp));
  std::cout << "Warping path:"
            << std::endl;
  std::cout << pretty_print(path, warp)
            << std::endl;
  std::cout << "- cost: "
            << dtw::get_path_cost(path, warp)
            << std::endl;
  std::cout << "- steps: "
            << path.size()
            << std::endl;

  return 0;
}
