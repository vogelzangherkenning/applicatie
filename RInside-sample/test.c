#include <RInside.h>

int main( int argc, char *argv[] ) {
	
	try {

		RInside R(argc, argv );

		R["filen"] = std::string(argv[1], strlen(argv[1]));

		std::string program = "library(tuneR)\n"
"library(seewave)\n"
"\n"
"data <- readWave(filen)\n"
"\n"
"spec <- spectro(data, plot=FALSE)\n"
"\n"
"rows <- dim(as.array(spec['amp'][[1]]))\n"
"columns <- dim(as.array(spec['time'][[1]]))\n"
"\n"
"resultMatrix <- matrix(numeric(0), nrow=columns, ncol=2)\n"
"\n"
"for(column in as.single(1:columns)) {\n"
"        startPos <- 1 + (column-1)*rows\n"
"        endPos <- column*rows\n"
"\n"
"        maxpos <- which.max(spec['amp'][[1]][startPos:endPos])\n"
"        maxfreq <- spec['freq'][[1]][maxpos];\n"
"\n"
"        time <- spec['time'][[1]][column];\n"
"\n"
"        resultMatrix[column,1] <- time\n"
"        resultMatrix[column,2] <- maxfreq\n"
"}\n"
"\n"
"resultMatrix";

		Rcpp::NumericMatrix m = R.parseEval(program);

		for( int i=0; i<m.nrow(); i++ ) {
			std::cout << m(i,0) << std::endl;
		}

	} catch( std::exception& ex ) {
		std::cerr << "Yow!" << std::endl;
	}

}
