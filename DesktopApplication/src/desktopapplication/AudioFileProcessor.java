
package desktopapplication;

import java.io.*;
import javax.sound.sampled.*;

class AudioFileProcessor {
    
  public static void main(String[] args) {
    copyAudio("myrecording.wav", "test.wav", 1.5, 3);
  }

  public static void copyAudio(String sourceFileName, String destinationFileName, double startSecond, double secondsToCopy) {
    AudioInputStream inputStream = null;
    AudioInputStream shortenedStream = null;
    try {
      File file = new File(sourceFileName);
      AudioFileFormat fileFormat = AudioSystem.getAudioFileFormat(file);
      AudioFormat format = fileFormat.getFormat();
      inputStream = AudioSystem.getAudioInputStream(file);
      int bytesPerSecond = format.getFrameSize() * (int)format.getFrameRate();
      inputStream.skip((long)(startSecond * bytesPerSecond));
      long framesOfAudioToCopy = (long)(secondsToCopy * (int)format.getFrameRate());
      shortenedStream = new AudioInputStream(inputStream, format, framesOfAudioToCopy);
      File destinationFile = new File(destinationFileName);
      AudioSystem.write(shortenedStream, fileFormat.getType(), destinationFile);
    } catch (Exception e) {
      println(e);
    } finally {
      if (inputStream != null) try { inputStream.close(); } catch (Exception e) { println(e); }
      if (shortenedStream != null) try { shortenedStream.close(); } catch (Exception e) { println(e); }
    }
  }

  public static void println(Object o) {
    System.out.println(o);
  }

  public static void print(Object o) {
    System.out.print(o);
  }
    public static double getDurationOfWavInSeconds(File file)
    {   
        AudioInputStream stream = null;

        try 
        {
            stream = AudioSystem.getAudioInputStream(file);

            AudioFormat format = stream.getFormat();

            return file.length() / format.getSampleRate() / (format.getSampleSizeInBits() / 8.0) / format.getChannels();
        }
        catch (Exception e) 
        {
            // log an error
            return -1;
        }
        finally
        {
            try { stream.close(); } catch (Exception ex) { }
        }
    }
}