/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package desktopapplication;

import javax.swing.JFrame;

/**
 *
 * @author Riccardo
 */
public class FrameController {
    
    private DesktopFrame frame;

    public FrameController() {
        frame = new DesktopFrame(this);
        loadRecorderPanel();
    }
    
    public void loadRecorderPanel(){
        RecorderPanel pnl = new RecorderPanel(this);
        pnl.setVisible(true);
        frame.clearPanel();
        frame.setPanel(pnl);
        frame.setVisible(true);
    }
    
    public void loadResulPanel(String result){
        ResultPanel pnl = new ResultPanel(this,result);
        pnl.setVisible(true);
        frame.clearPanel();
        frame.setPanel(pnl);
        frame.setVisible(true);
    }
}

