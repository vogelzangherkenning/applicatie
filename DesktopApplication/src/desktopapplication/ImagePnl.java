/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package desktopapplication;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicToolBarUI;

/**
 *
 * @author Riccardo
 */
public class ImagePnl extends JPanel{
    public RangeSlider slider;
    public JLabel lblWave;
    public JPanel pnlImageHolder;
    
    public ImagePnl(){
        slider = new RangeSlider(0, 100);
        lblWave = new JLabel("");
        
        this.setLayout(new BorderLayout());
        this.add(slider,BorderLayout.NORTH);
        pnlImageHolder = new JPanel();
        pnlImageHolder.setLayout(new BorderLayout());
        pnlImageHolder.setPreferredSize(new Dimension(100, 100));
        lblWave.setPreferredSize(new Dimension(100, 100));
        lblWave.setMaximumSize(new Dimension(100, 100));
        pnlImageHolder.setMaximumSize(new Dimension(100, 100));
        pnlImageHolder.add(lblWave);
        this.add(pnlImageHolder);
    }
    public void refresh(){
        pnlImageHolder.repaint();
    }
}
