/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package desktopapplication;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

/**
 *
 * @author Riccardo
 */
public class Recorder {
    boolean stopCapture = false;
    ByteArrayOutputStream byteArrayOutputStream;
    AudioFormat audioFormat;
    TargetDataLine targetDataLine;
    AudioInputStream audioInputStream;
    SourceDataLine sourceDataLine;
    File file=new File("myrecording.wav");
    FileOutputStream fout;
    AudioFileFormat.Type fileType;

    public Recorder() {
        try {
            fout=new FileOutputStream(file);
	} catch (FileNotFoundException e1) {	
            e1.printStackTrace();
	}
    }
    public void setFile(String path)
    {
        file = new File(path);
    }
    
    public void play(){
        try{
         /*
         AudioInputStream audioInputStream =AudioSystem.getAudioInputStream(this.getClass().getResource("myrecording.wav"));
         Clip clip = AudioSystem.getClip();
         clip.open(audioInputStream);
         clip.start( );*/
         WavePlayer aw = new WavePlayer(file.getAbsolutePath());
         aw.start();
         
        }
       catch(Exception ex)
       {
           System.out.println(ex.getMessage());
       }
    }
    
    
    public void stopCapture(){
        try{
            Thread.sleep(1000);
        }catch (InterruptedException ie){
            System.err.println(ie.getMessage());
        }
        
        stopCapture = true;
    }
    public void captureAudio(){
        try{
          //Get everything set up for
          // capture
          audioFormat = getAudioFormat();
          DataLine.Info dataLineInfo = new DataLine.Info(TargetDataLine.class,audioFormat);
          targetDataLine = (TargetDataLine) AudioSystem.getLine(dataLineInfo);
          targetDataLine.open(audioFormat);
          targetDataLine.start();

          //Create a thread to capture the
          // microphone data and start it
          // running.  It will run until
          // the Stop button is clicked.
          Thread captureThread = new Thread(new CaptureThread());
          captureThread.start();
        } catch (Exception e) {
          System.out.println(e);
          System.exit(0);
        }//end catch
    }//end captureAudio method
    private AudioFormat getAudioFormat(){
      float sampleRate = 8000.0F;
      //8000,11025,16000,22050,44100
      int sampleSizeInBits = 16;
      //8,16
      int channels = 1;
      //1,2
      boolean signed = true;
      //true,false
      boolean bigEndian = false;
      //true,false
      return new AudioFormat(sampleRate,sampleSizeInBits,channels,signed,bigEndian);
    }
  
  
public void saveAudio() {
    try{
      //Get everything set up for
      // playback.
      //Get the previously-saved data
      // into a byte array object.
      byte audioData[] = byteArrayOutputStream.toByteArray();
      //Get an input stream on the
      // byte array containing the data
      InputStream byteArrayInputStream = new ByteArrayInputStream(audioData);
      AudioFormat audioFormat = getAudioFormat();
      audioInputStream = new AudioInputStream(byteArrayInputStream,audioFormat,audioData.length/audioFormat.getFrameSize());
      DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class,audioFormat);
      sourceDataLine = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
      sourceDataLine.open(audioFormat);
      sourceDataLine.start();

      //Create a thread to play back
      // the data and start it
      // running.  It will run until
      // all the data has been played
      // back.
      Thread saveThread = new Thread(new SaveThread());
      saveThread.start();
    } catch (Exception e) {
      System.out.println(e);
      System.exit(0);
    }//end catch
  }//end playAudio    
    
class SaveThread extends Thread{
     byte tempBuffer[] = new byte[10000];

     public void run(){
       try{
         int cnt;
         //Keep looping until the input
         // read method returns -1 for
         // empty stream.

          if (AudioSystem.isFileTypeSupported(AudioFileFormat.Type.WAVE, 
                 audioInputStream)) {
               AudioSystem.write(audioInputStream, AudioFileFormat.Type.WAVE, file);
          } 
          sourceDataLine.stop();
          sourceDataLine.close();
       }catch (Exception e) {
         System.out.println(e);
         System.exit(0);
       }//end catch
     }//end run
   }
 
class CaptureThread extends Thread{
      //An arbitrary-size temporary holding
      // buffer
      byte tempBuffer[] = new byte[10000];
      public void run(){
        byteArrayOutputStream = new ByteArrayOutputStream();
        stopCapture = false;
        try{//Loop until stopCapture is set
            // by another thread that
            // services the Stop button.
          while(!stopCapture){
            //Read data from the internal
            // buffer of the data line.
            int cnt = targetDataLine.read(tempBuffer,0,tempBuffer.length);
            if(cnt > 0){
              //Save data in output stream
              // object.
              byteArrayOutputStream.write(tempBuffer, 0, cnt);

            }//end if
          }//end while
          int cnt = targetDataLine.read(tempBuffer,0,tempBuffer.length);
          if(cnt > 0){
              //Save data in output stream
              // object.
            byteArrayOutputStream.write(tempBuffer, 0, cnt);

          }//end if
          byteArrayOutputStream.close();
          targetDataLine.stop();
          targetDataLine.close();
          
        }catch (Exception e) {
          System.out.println(e);
          System.exit(0);
        }//end catch
      }//end run
    }

    public static void copyFile(String old, String newf){
        File oldFile = new File(old);
        File newfile = new File(newf);
        try{
            newfile.delete();
            Files.copy(oldFile.toPath(), newfile.toPath());
        }catch (IOException ie) {
            System.err.println(ie.getMessage());
        }
    }
}
