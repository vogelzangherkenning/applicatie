/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package desktopapplication;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JPanel;

/**
 *
 * @author Riccardo
 */
public class DesktopFrame extends JFrame{
    
    //private DeskMenu menubar;
    private FrameController controller;
    private JPanel currentPanel;

    public DesktopFrame(FrameController controller) {
        this.controller = controller;
        showGUI();
    }
    
    private void showGUI(){
        initializeComponents();
        drawFrame();
    }
    
    private void initializeComponents(){
        this.setLayout(new BorderLayout());
        currentPanel = new JPanel();
        this.add(currentPanel);
        
        
        
    }
    
    private void drawFrame(){
        setTitle("VogelZang");
        setSize(1000, 600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }
    public JPanel getPanel(){
        return currentPanel;
    }
    public void setPanel(JPanel panel){
        this.currentPanel = panel;
        this.add(panel);
    }
    public void clearPanel(){
        currentPanel.setVisible(false);
        this.remove(currentPanel);
    }
    
}
